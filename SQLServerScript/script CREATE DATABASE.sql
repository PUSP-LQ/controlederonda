SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[ob_OrdemDeServicoDetalheTO]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[ob_OrdemDeServicoDetalheTO](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [varchar](36) NULL,
	[OrdemDeServicoUID] [varchar](36) NULL,
	[Contador] [int] NULL,
	[Interacao] [text] NULL,
	[Momento] [datetime] NULL,
	[Autor] [varchar](36) NULL,
	[Atendente] [varchar](36) NULL,
	[SecaoOrigem] [varchar](36) NULL,
	[SecaoDestino] [varchar](36) NULL,
 CONSTRAINT [PK_ob_ChamadoDetalheTO] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[ob_OrdemDeServicoTO]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[ob_OrdemDeServicoTO](
	[UID] [varchar](36) NULL,
	[ID] [int] IDENTITY(98,1) NOT NULL,
	[Codigo] [varchar](20) NULL,
	[POP] [varchar](26) NULL,
	[Assunto] [varchar](120) NULL,
	[Necessidade] [varchar](26) NULL,
	[Local] [varchar](120) NULL,
	[Emergencia] [smallint] NULL,
	[Justificativa] [varchar](200) NULL,
	[SolicitanteNumeroUSP] [varchar](50) NULL,
	[SolicitanteNome] [varchar](120) NULL,
	[SolicitanteUID] [varchar](36) NULL,
	[SolicitanteEmail] [varchar](120) NULL,
	[SolicitanteRamal] [varchar](40) NULL,
	[SolicitanteUnidade] [varchar](60) NULL,
	[SolicitanteSecao] [varchar](60) NULL,
	[SecaoPOP] [varchar](50) NULL,
	[SecaoExecutante] [varchar](50) NULL,
	[LinkSuperior] [varchar](36) NULL,
	[MomAbertura] [datetime] NULL,
	[MomCadastro] [datetime] NULL,
	[FluxoAtual] [varchar](50) NULL,
	[Ano] [char](4) NULL,
	[Contador] [int] NULL,
	[Relatorio] [varchar](12) NULL,
	[MOMALT] [timestamp] NULL,
	[SatisfacaoServico] [int] NULL,
	[UnidadeExecutante] [varchar](50) NULL,
	[MomExecutado] [datetime] NULL,
	[CienteServicoRealizado] [datetime] NULL,
	[ConcordaServicoRealizado] [smallint] NULL,
 CONSTRAINT [PK_ob_chamado] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [IX_ob_ChamadoTO_Codigo] UNIQUE NONCLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [IX_ob_ChamadoTO_UID] UNIQUE NONCLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[if_ChamadoTecnicoTO]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[if_ChamadoTecnicoTO](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [varchar](36) NOT NULL,
	[Codigo] [varchar](20) NOT NULL,
	[Servico] [varchar](120) NULL,
	[FluxoAtual] [varchar](120) NULL,
	[SolicitanteUID] [varchar](36) NOT NULL,
	[SolicitanteSecao] [varchar](60) NOT NULL,
	[MomCadastro] [datetime] NOT NULL,
	[Local] [varchar](120) NULL,
	[Patrimonio] [varchar](60) NULL,
	[Ano] [char](4) NOT NULL,
	[Contador] [int] NOT NULL,
	[Relatorio] [varchar](12) NULL,
	[Prioridade] [varchar](50) NULL,
	[Atendente] [varchar](36) NULL,
	[CienteServicoRealizado] [datetime] NULL,
	[ConcordaServicoRealizado] [char](3) NULL,
	[SatisfacaoServico] [int] NULL,
	[Categoria] [varchar](60) NULL,
	[AtendimentoCod] [varchar](50) NULL,
	[Ocorrencia] [varchar](160) NULL,
	[AtendOcorrUID] [varchar](36) NULL,
	[DecorrenteDe] [varchar](50) NULL,
 CONSTRAINT [PK_if_ChamadoTecnicoTO] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [IX_if_ChamadoTecnicoTO_Codigo] UNIQUE NONCLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [IX_if_ChamadoTecnicoTO_UID] UNIQUE NONCLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[if_ChamadoTecnicoDetalheTO]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[if_ChamadoTecnicoDetalheTO](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [varchar](36) NULL,
	[ChamadoTecnicoUID] [varchar](36) NULL,
	[Contador] [int] NULL,
	[Interacao] [text] NULL,
	[Momento] [datetime] NULL,
	[Autor] [varchar](36) NULL,
	[Atendente] [varchar](36) NULL,
	[SecaoOrigem] [varchar](36) NULL,
	[SecaoDestino] [varchar](36) NULL,
	[Fluxo] [varchar](36) NULL,
 CONSTRAINT [PK_ob_ChamadoDetalheTOID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[ob_LogTO]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[ob_LogTO](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Objeto] [nchar](60) NULL,
	[Usuario] [nchar](36) NULL,
	[IP] [nchar](16) NULL,
	[Excecao] [bit] NULL,
	[RegistroLog] [text] NULL,
	[Local] [varchar](100) NULL,
	[Momento] [datetime] NULL,
	[ObjetoUID] [varchar](36) NULL,
	[Browser] [varchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[if_PatrimonioTO]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[if_PatrimonioTO](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Numpat] [char](10) NULL,
	[Sglcendsp] [varchar](50) NULL,
	[Nomcendsp] [varchar](50) NULL,
	[Datadoc] [datetime] NULL,
	[Codpes] [varchar](10) NULL,
	[Nompes] [varchar](60) NULL,
	[Nomsgpitmmat] [varchar](50) NULL,
	[Stabem] [varchar](10) NULL,
	[Codlocusp] [varchar](10) NULL,
	[MOMCAD] [datetime] NULL,
	[Epfmarpat] [varchar](60) NULL,
	[Numidfpat] [varchar](60) NULL,
	[Tippat] [varchar](60) NULL,
	[Modpat] [varchar](60) NULL,
 CONSTRAINT [PK_if_PatrimonioTO_2__55] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[if_AtendimentoTO]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[if_AtendimentoTO](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [varchar](36) NULL,
	[Codigo] [varchar](50) NULL,
	[Atendimento] [varchar](160) NULL,
	[Ocorrencia] [varchar](160) NULL,
 CONSTRAINT [PK_if_AtendimentoTO_1__53] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[if_LaudoTO]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[if_LaudoTO](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_if_LaudoTO_UID_1__54]  DEFAULT (newid()),
	[PatrimonioUID] [varchar](36) NULL,
	[ChamadoUID] [varchar](36) NULL,
	[Condicao] [varchar](26) NULL,
	[Destino] [varchar](26) NULL,
	[Obs] [varchar](60) NULL,
 CONSTRAINT [PK_if_LaudoTO_2__54] PRIMARY KEY NONCLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[ob_local]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[ob_local](
	[codigo] [varchar](20) NOT NULL,
	[descricao] [varchar](60) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[ob_logs]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[ob_logs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[objeto] [nchar](60) NULL,
	[usuario] [nchar](36) NULL,
	[momento] [datetime] NULL,
	[ip] [nchar](16) NULL,
	[excecao] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[ob_necessidade]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[ob_necessidade](
	[codigo] [varchar](26) NOT NULL,
	[descricao] [varchar](40) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[ob_servico]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[ob_servico](
	[codigo] [varchar](36) NOT NULL,
	[descricao] [varchar](120) NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[ob_userrecover]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[ob_userrecover](
	[uid] [varchar](36) NOT NULL,
	[id] [bigint] IDENTITY(32,1) NOT NULL,
	[pessoa] [varchar](36) NULL,
	[momento] [datetime] NULL,
	[codigo] [varchar](36) NULL,
	[visualizou] [tinyint] NULL,
	[host] [varchar](36) NULL,
	[ip] [varchar](36) NULL,
	[altmom] [datetime] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[ac_formaviso]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[ac_formaviso](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uid] [varchar](36) NOT NULL,
	[setorcontratante] [varchar](120) NULL,
	[responsavel] [varchar](120) NULL,
	[respramal] [varchar](120) NULL,
	[emcontrata] [varchar](120) NULL,
	[emcontato] [varchar](120) NULL,
	[emservico] [varchar](120) NULL,
	[preventradata] [varchar](120) NULL,
	[preventrahora] [varchar](120) NULL,
	[prevsaidata] [varchar](120) NULL,
	[prevsaihora] [varchar](120) NULL,
	[realentradata] [varchar](120) NULL,
	[realentrahora] [varchar](120) NULL,
	[realsaidata] [varchar](120) NULL,
	[realsaihora] [varchar](120) NULL,
	[cadmom] [datetime] NULL,
	[momenvioemail] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[ac_destinatarios]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[ac_destinatarios](
	[uid] [varchar](36) NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[email] [varchar](120) NULL,
	[tabenvio] [varchar](120) NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[ce_material]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[ce_material](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uid] [uniqueidentifier] NULL,
	[nome] [varchar](120) NULL,
	[codbemmercurio] [int] NULL,
	[descricao] [text] NULL,
 CONSTRAINT [PK_ce_material_2__57] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[gu_RondaTurnoTO]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[gu_RondaTurnoTO](
	[UID] [varchar](36) NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TurnoInicio] [smalldatetime] NULL,
	[TurnoFim] [smalldatetime] NULL,
	[NomeContrRadio] [varchar](50) NULL,
	[MOMCAD] [smalldatetime] NULL,
	[MOMALT] [smalldatetime] NULL,
	[UsuarioLogadoUID] [varchar](36) NOT NULL,
	[UsuarioLogadoNome] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[gu_RondaRegistrosTO]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[gu_RondaRegistrosTO](
	[UID] [varchar](36) NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RondaTurnoUID] [varchar](36) NOT NULL,
	[RondaVeiculo] [nchar](10) NULL,
	[RondaEquipe] [nchar](10) NULL,
	[Local01] [varchar](50) NULL,
	[Local02] [varchar](50) NULL,
	[Local03] [varchar](50) NULL,
	[Local04] [varchar](50) NULL,
	[MOMCAD] [smalldatetime] NULL,
	[MOMALT] [smalldatetime] NULL,
	[UsuarioLogadoUID] [varchar](36) NULL,
	[UsuarioLogadoNome] [varchar](50) NULL,
 CONSTRAINT [PK_gu_RondaTO] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[ce_patrimonio]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[ce_patrimonio](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uid] [varbinary](36) NULL,
	[codmaterial] [varchar](36) NULL,
	[cadmom] [datetime] NULL,
	[codpatrimonio] [varchar](12) NULL,
	[codpessoa] [varchar](36) NULL,
 CONSTRAINT [PK_ce_patrimonio_1__57] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[ce_logpatrimonio]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[ce_logpatrimonio](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[momento] [datetime] NULL,
	[codpessoaorigem] [varchar](36) NULL,
	[codpessoadestino] [varchar](36) NULL,
 CONSTRAINT [PK_ce_logpatrimonio_1__57] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[ob_acaocaduco]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[ob_acaocaduco](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uidchamado] [varchar](36) NULL,
	[momenvioalerta] [datetime] NULL,
	[momrespondeuusuario] [datetime] NULL,
	[acaoselecionadausuario] [varchar](20) NULL,
	[xhost] [varchar](40) NULL,
	[xip] [varchar](15) NULL,
	[momcaducoualerta] [datetime] NULL,
 CONSTRAINT [PK_ob_acaocaduco_1__55] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[pj_fasesdoprojeto]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[pj_fasesdoprojeto](
	[codigo] [varchar](50) NOT NULL,
	[descricao] [varchar](50) NULL,
	[ordem] [int] NULL,
	[cadmom] [datetime] NULL,
	[xhost] [varchar](50) NULL,
	[cadusuario] [varchar](50) NULL,
 CONSTRAINT [PK_pj_fasesdoprojeto_1__56] PRIMARY KEY NONCLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[pj_projeto]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[pj_projeto](
	[uid] [varchar](36) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[unidade] [varchar](50) NULL,
	[secao] [varchar](50) NULL,
	[codigoproc] [varchar](50) NULL,
	[docbase] [varchar](50) NULL,
	[nomeprojeto] [varchar](100) NULL,
	[assunto] [varchar](150) NULL,
	[nomepessoainte] [varchar](100) NULL,
	[cadmom] [datetime] NULL,
	[usercreate] [varchar](36) NULL,
	[momabertura] [datetime] NULL,
	[canmom] [datetime] NULL,
 CONSTRAINT [PK_pj_projeto_1__57] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[pj_projetodetalhe]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[pj_projetodetalhe](
	[uid] [varchar](36) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uidprojeto] [varchar](36) NULL,
	[parecer] [text] NULL,
	[uidpessoadestino] [varchar](36) NULL,
	[unidadedestino] [varchar](50) NULL,
	[secaodestino] [varchar](50) NULL,
	[cadmom] [datetime] NULL,
	[faseatual] [varchar](36) NULL,
	[usercreate] [varchar](36) NULL,
	[contador] [int] NULL,
	[pessoadestino] [varchar](150) NULL,
	[emaildestino] [varchar](150) NULL,
	[xhost] [varchar](15) NULL,
 CONSTRAINT [PK_pj_projetodetalhe_1__57] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[pj_pessoainteresse]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[pj_pessoainteresse](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uid] [varchar](36) NULL,
	[uidpessoa] [varchar](36) NULL,
	[uidprojeto] [varchar](36) NULL,
	[papel] [varchar](36) NULL,
	[cadmom] [datetime] NULL,
 CONSTRAINT [PK_pj_pessoainteresse_1__57] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[si_patrimoniodetalhe]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[si_patrimoniodetalhe](
	[codigopatrimonio] [varchar](20) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[valor] [varchar](40) NULL,
	[caracteristica] [varchar](40) NULL,
	[cadmom] [datetime] NULL,
 CONSTRAINT [PK_si_patrimoniodetalhe_1__55] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[si_patrimonio]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[si_patrimonio](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codigo] [varchar](20) NULL,
	[bemmaterial] [varchar](50) NULL,
	[uidpessoa] [varchar](36) NULL,
	[codsecao] [varchar](36) NULL,
	[local] [varchar](120) NULL,
	[cadmom] [datetime] NULL,
	[usercreate] [varchar](40) NULL,
	[xhost] [varchar](20) NULL,
	[observacao] [text] NULL,
 CONSTRAINT [PK_si_patrimonio_2__55] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[si_chamadotecnico]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[si_chamadotecnico](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uid] [varchar](36) NULL,
	[codigo] [varchar](36) NULL,
	[uidpessoa] [varchar](36) NULL,
	[nomesolicitante] [varchar](100) NULL,
	[codsecao] [varchar](36) NULL,
	[nomesecao] [varchar](60) NULL,
	[siglasecao] [varchar](30) NULL,
	[tipochamado] [varchar](36) NULL,
	[local] [varchar](120) NULL,
	[atendente] [varchar](36) NULL,
	[assunto] [varchar](120) NULL,
	[xcadmom] [datetime] NULL,
	[xusercreate] [varchar](36) NULL,
	[xhostcreate] [varchar](36) NULL,
	[momfecha] [datetime] NULL,
	[patrimonio] [varchar](50) NULL,
	[qualidadetempo] [nvarchar](10) NULL,
	[qualidadesolucao] [nchar](10) NULL,
	[qualidadecomentario] [varchar](200) NULL,
 CONSTRAINT [PK_si_chamadotecnico_2__54] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[si_chamadotecnicodetalhe]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[si_chamadotecnicodetalhe](
	[uid] [varchar](36) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[contador] [int] NULL,
	[chamado] [varchar](36) NULL,
	[interacao] [text] NULL,
	[momento] [datetime] NULL,
	[uidpessoa] [varchar](36) NULL,
	[autor] [varchar](36) NULL,
	[IP_SOLIC] [varchar](36) NULL,
 CONSTRAINT [PK_si_chamadotecnicodetalhe_1__54] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[ob_DVCampusTO]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[ob_DVCampusTO](
	[UID] [varchar](36) NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Codigo] [varchar](36) NOT NULL,
	[Superior] [varchar](20) NULL,
	[Descricao] [varchar](60) NOT NULL,
	[Sigla] [varchar](30) NULL,
	[Pessoa] [varchar](36) NULL,
	[Tipo] [varchar](20) NULL,
	[Telefone] [varchar](15) NULL,
	[Email] [varchar](100) NULL,
	[EmailAntigo] [varchar](100) NULL,
	[xCADUSU] [varchar](60) NULL,
	[xCADIP] [varchar](60) NULL,
	[xCADHOST] [varchar](60) NULL,
	[MOMCAD] [datetime] NULL,
	[xALTUSU] [varchar](60) NULL,
	[xALTIP] [varchar](60) NULL,
	[xALTHOST] [varchar](60) NULL,
	[MOMALT] [datetime] NULL,
	[Numero] [int] NULL,
	[Unidade] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[ob_EmailTO]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[ob_EmailTO](
	[ID] [int] IDENTITY(101,1) NOT NULL,
	[UID] [varchar](36) NOT NULL,
	[Remetente] [varchar](100) NULL,
	[Titulo] [varchar](100) NULL,
	[Mensagem] [ntext] NULL,
	[Destinatario] [varchar](250) NULL,
	[Enviar] [datetime] NULL,
	[Enviado] [datetime] NULL,
	[MOMCAD] [datetime] NULL,
	[Sistema] [varchar](50) NULL,
 CONSTRAINT [PK_ob_EmailTO] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[ob_NewsTO]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[ob_NewsTO](
	[UID] [varchar](36) NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Assunto] [varchar](50) NULL,
	[Descricao] [varchar](180) NULL,
	[MomCadastro] [datetime] NULL,
 CONSTRAINT [PK_ob_news] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[ob_PendenciaTO]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[ob_PendenciaTO](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [varchar](36) NULL,
	[Objeto] [varchar](50) NULL,
	[ObjetoUID] [varchar](36) NULL,
	[UsuarioNumeroUSPDestino] [varchar](36) NULL,
	[UsuarioUIDDestino] [varchar](36) NULL,
	[UsuarioUIDOrigem] [varchar](36) NULL,
	[Tipo] [varchar](50) NULL,
	[Concluido] [datetime] NULL,
	[Inicia] [datetime] NULL,
	[Expira] [datetime] NULL,
	[MOMCAD] [datetime] NULL,
 CONSTRAINT [PK_PendenciaTO] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[ob_PessoaTO]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[ob_PessoaTO](
	[UID] [varchar](36) NOT NULL,
	[ID] [int] IDENTITY(12,1) NOT NULL,
	[Nome] [varchar](100) NULL,
	[Usuario] [varchar](40) NULL,
	[NumeroUSP] [varchar](100) NULL,
	[Apresentacao] [varchar](60) NULL,
	[Senha] [varchar](32) NULL,
	[SecaoOuDepto] [varchar](100) NULL,
	[Unidade] [varchar](100) NULL,
	[Sexo] [char](2) NULL,
	[Telefone] [varchar](20) NULL,
	[Email] [varchar](100) NULL,
	[Nascimento] [smalldatetime] NULL,
	[Atende] [varchar](20) NULL,
	[Sistemas] [nchar](10) NULL,
	[CodigoVerificacao] [nchar](10) NULL,
	[CadastroConfirmado] [datetime] NULL,
	[CadastroAtivo] [datetime] NULL,
	[LinkSuperior] [varchar](36) NULL,
	[MOMCAD] [datetime] NULL,
	[MOMALT] [datetime] NULL,
	[IP] [varchar](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[ob_POPTO]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[ob_POPTO](
	[Codigo] [varchar](26) NOT NULL,
	[Descricao] [varchar](40) NOT NULL,
	[DVCampusCodigo] [nvarchar](36) NULL,
	[PessoaAtendeUID] [varchar](36) NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[ob_RelatorioTO]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[ob_RelatorioTO](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoRelatorio] [varchar](10) NULL,
	[MomAbertura] [datetime] NULL,
	[MomFechamento] [datetime] NULL,
	[Qtd] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[ob_ControleFluxoTO]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[ob_ControleFluxoTO](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [varchar](36) NULL,
	[Codigo] [varchar](50) NULL,
	[Momento] [datetime] NULL,
	[Objeto] [varchar](50) NULL,
	[ObjetoUID] [varchar](36) NULL,
	[UsuarioUID] [varchar](36) NULL,
 CONSTRAINT [PK_ob_ControleFluxoTO] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[ob_MaoObraTO]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[ob_MaoObraTO](
	[UID] [varchar](36) NOT NULL,
	[ID] [bigint] IDENTITY(16,1) NOT NULL,
	[OrdemDeServicoUID] [varchar](36) NOT NULL,
	[MomIni] [datetime] NULL,
	[MomFin] [datetime] NULL,
	[QtdHr] [decimal](10, 2) NULL,
	[ValorUnit] [decimal](10, 2) NULL,
	[QtdFuncionarios] [int] NULL,
 CONSTRAINT [PK_ob_MaoObraTO] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[ob_MaoObraTrabalhadorTO]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[ob_MaoObraTrabalhadorTO](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [varchar](36) NULL,
	[OrdemDeServicoUID] [varchar](36) NULL,
	[PessoaUID] [varchar](36) NULL,
	[MOMCAD] [datetime] NULL,
 CONSTRAINT [PK_ob_TrabalhadorObraTO] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[w3oscclq].[ob_MaterialTO]') AND type in (N'U'))
BEGIN
CREATE TABLE [w3oscclq].[ob_MaterialTO](
	[UID] [varchar](36) NOT NULL,
	[ID] [bigint] IDENTITY(32,1) NOT NULL,
	[OrdemDeServicoUID] [varchar](36) NOT NULL,
	[Descricao] [varchar](60) NOT NULL,
	[Qtd] [decimal](10, 2) NULL,
	[Unidade] [varchar](40) NULL,
	[ValorUnitario] [decimal](10, 2) NULL,
 CONSTRAINT [PK_ob_MaterialTO] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
