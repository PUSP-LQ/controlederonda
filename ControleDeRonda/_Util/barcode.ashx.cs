﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp.text.pdf;
using System.Drawing;
using System.Drawing.Imaging;

namespace SistemasOnline
{
    public class barcode : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            string code = context.Request.QueryString["code"];

            context.Response.ContentType = "image/GIF";
            Barcode128 b = new Barcode128();
            b.Code = code;
            b.BarHeight = 30;
            b.N = 3;
            b.CreateDrawingImage(Color.Black, Color.White).Save(
                context.Response.OutputStream,
                ImageFormat.Gif);

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
