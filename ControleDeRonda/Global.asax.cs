﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Xml.Linq;
using CamadaSQLServer.Orbita;

namespace SistemasOnline
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            try
            {
                Exception ex = Server.GetLastError();
                LogTO log = new LogTO();
                log.Momento = DateTime.Now;
                log.RegistroLog = ex.InnerException + " " + ex.Message;
                log.Excecao = true;
                log.IP = Request.UserHostAddress;
                log.Usuario = HttpContext.Current.User.Identity.Name;
                log.Local = Request.Path;
                log.Browser = Request.Browser.ToString();
                LogDAO.Insert(log);
            }
            catch (Exception)
            {
//lancar txt
            }  
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}