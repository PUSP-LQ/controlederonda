﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxNavBar;
using CamadaSQLServer.Orbita;

namespace ControleDeRonda
{
    public partial class MasterOrbita : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                Server.Transfer("~/default.aspx");    //enviar para página sua sessão expirou            
            }          
        }

        protected void LoginStatus1_LoggedOut(object sender, EventArgs e)
        {
            Session.Abandon();
        }
    }

}
