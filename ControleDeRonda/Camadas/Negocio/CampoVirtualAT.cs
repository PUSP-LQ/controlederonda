﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CamadaSQLServer.Orbita
{
    [System.AttributeUsage(System.AttributeTargets.Property)]
    public class CampoVirtual : System.Attribute
    {
        public bool campoVirtual;

        public CampoVirtual(bool campovirtual)
        {
            this.campoVirtual = campovirtual;
        }
    }

    public enum Alvo { Orbita, Informatica, Transporte, Energia, Guarda };

    [System.AttributeUsage(System.AttributeTargets.Class)]
    public class Sistema : System.Attribute
    {        
        public Alvo sistemaAlvo;

        public Sistema(Alvo alvo)
        {
            this.sistemaAlvo = alvo;
        }
    }
}
