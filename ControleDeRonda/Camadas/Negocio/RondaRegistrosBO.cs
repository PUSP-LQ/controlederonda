﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace ControleDeRonda.Camadas
{

    [DataObject]
    public class RondaRegistrosBO
    {       
        public List<RondaRegistrosTO> getRegistrosTurno(string uidTurno)
        {
            return RondaRegistrosDAO.ExecSQL("select * from gu_RondaRegistrosTO where RondaTurnoUID = '" + uidTurno + "' order by MOMCAD, RondaEquipe, RondaVeiculo, ID DESC", new RondaRegistrosTO()).ToList<RondaRegistrosTO>();
        }
    }
}
