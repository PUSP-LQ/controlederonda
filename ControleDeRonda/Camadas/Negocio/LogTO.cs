﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CamadaSQLServer.Orbita
{
    public class LogTO
    {
        public int ID { get; set; }
        public string Objeto { get; set; }
        public string ObjetoUID { get; set; }
        public string Browser { get; set; }
        public string Usuario { get; set; }
        public string IP { get; set; }
        public bool Excecao { get; set; }
        public string RegistroLog { get; set; }
        public string Local { get; set; }
        public DateTime Momento { get; set; }

        //contrutor
        public LogTO()
        {
            //this.Momento = DateTime.Now;
        }

    }
}
