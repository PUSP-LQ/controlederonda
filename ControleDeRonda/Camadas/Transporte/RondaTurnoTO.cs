﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CamadaSQLServer.Orbita;

namespace ControleDeRonda.Camadas
{
    [Sistema(Alvo.Guarda)]
    public class RondaTurnoTO
    {
        public int ID { get; set; }
        public string UID { get; set; }

        //[CampoVirtual(true)]
        public DateTime MOMCAD { get; set; }

        //[CampoVirtual(true)]
        public DateTime MOMALT { get; set; }

        public DateTime TurnoInicio { get; set; }
        public DateTime TurnoFim { get; set; }
        public string NomeContrRadio { get; set; }

        public string UsuarioLogadoUID { get; set; }
        public string UsuarioLogadoNome { get; set; }


        public RondaTurnoTO()
        {
            this.UID = Guid.NewGuid().ToString();
            this.TurnoInicio = DateTime.Now;
            this.MOMCAD = DateTime.Now;
            this.MOMALT = DateTime.Now;
        }

        public RondaTurnoTO(string uid)
        {
            RondaTurnoTO turno = new RondaTurnoTO();
            turno.UID = uid;
            turno = RondaTurnoDAO.Get(turno);

            this.ID = turno.ID;
            this.NomeContrRadio = turno.NomeContrRadio;
            this.TurnoFim = turno.TurnoFim;
            this.TurnoInicio = turno.TurnoInicio;
            this.UID = turno.UID;
            this.UsuarioLogadoNome = turno.UsuarioLogadoNome;
            this.UsuarioLogadoUID = turno.UsuarioLogadoUID;

        }

    }
}
