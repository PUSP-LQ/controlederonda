﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using CamadaSQLServer.Orbita;
using System.Data.SqlClient;

namespace ControleDeRonda.Camadas
{
    [DataObject]   
    public class RondaTurnoDAO : GenericoDAO<RondaTurnoTO>
    {
        static string connectionString = System.Configuration.ConfigurationSettings.AppSettings["conn"];

        public static RondaTurnoTO getLastTurno()
        {
            string sql = "select top 1 * from gu_RondaTurnoTO where TurnoFim is null order by id";

            RondaTurnoTO turno = new RondaTurnoTO();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                //cmd.Parameters.Add(new SqlParameter("@id", id));
                cmd.CommandText = sql;
                cmd.Connection = conn;

                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    turno = new RondaTurnoTO()
                    {
                        UID = dr["UID"].ToString(),
                        ID = (int)dr["ID"]   
                    };

                    if (dr["MOMALT"] != DBNull.Value)
                        turno.MOMALT = (DateTime)dr["MOMALT"];

                    if (dr["MOMCAD"] != DBNull.Value)
                        turno.MOMCAD = (DateTime)dr["MOMCAD"];
                        
                    if (dr["NomeContrRadio"] != DBNull.Value)
                        turno.NomeContrRadio = dr["NomeContrRadio"].ToString();

                    if (dr["TurnoInicio"] != DBNull.Value)
                        turno.TurnoInicio =  (DateTime)dr["TurnoInicio"];
                    
                    if (dr["UsuarioLogadoNome"] != DBNull.Value)
                        turno.UsuarioLogadoNome = dr["UsuarioLogadoNome"].ToString();

                    if (dr["UsuarioLogadoUID"] != DBNull.Value)
                        turno.UsuarioLogadoUID = dr["UsuarioLogadoUID"].ToString();
                    

                }
            }
            return turno;

        }
    }
}
