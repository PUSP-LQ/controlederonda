﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Data.SqlClient;

namespace CamadaSQLServer.Orbita
{
    public class GenericoDAO<T> where T : new()
    {
        //public int gety()
        //{
        //    List<PessoaTO> lista = GenericoDAO<PessoaTO>.Select(new PessoaTO());
        //    return 1;
        //}

        public static T Get(T objeto)
        {
            List<T> lista = GenericoDAO<T>.Select(objeto);

            if (lista.Count > 0)
                return lista[0];
            else
                return default(T);
        }

        public static T GetLast(T objeto)
        {
            List<T> lista = GenericoDAO<T>.Select(objeto);

            return lista.LastOrDefault<T>();
        }

        public static List<T> SelectLike(T objeto)
        {
            return GenericoDAO<T>.select(objeto, true);
        }

        public static List<T> Select(T objeto)
        {
            return GenericoDAO<T>.select(objeto, false);
        }

        private static List<T> select(T objeto, bool like)
        {
            //Alvo sistemaAlvo = Alvo.Orbita;

            //foreach (Attribute attr in objeto.GetType().GetCustomAttributes(true))
            //{
            //    Sistema sistema = attr as Sistema;
            //    if (sistema != null)
            //    {
            //        sistemaAlvo = sistema.sistemaAlvo;
            //    }
            //}

            string connectionString = System.Configuration.ConfigurationSettings.AppSettings["conn"];
            string tabela = objeto.GetType().Name;
            string prefixoTabela = string.Empty;// SistemaBO.getPrefixoTabela(sistemaAlvo);
            string sql = "select * from " + prefixoTabela + tabela + " where 1 = 1 ";
            string sqlFields = string.Empty;
            string sqlParameters = string.Empty;

            DateTime datasInicializadasNaoNulas = new DateTime();

            foreach (PropertyInfo item in objeto.GetType().GetProperties())
            {
                bool campoVirtual = false;

                foreach (Attribute attr in item.GetCustomAttributes(true))
                {
                    CampoVirtual campo = attr as CampoVirtual;
                    if (campo != null)
                    {
                        campoVirtual = campo.campoVirtual;
                    }
                }

                if (!campoVirtual && item.CanRead && item.GetValue(objeto, null) != null && !string.IsNullOrEmpty(item.GetValue(objeto, null).ToString()) && item.GetValue(objeto, null).ToString() != "0")
                {
                    if (item.PropertyType == typeof(DateTime) && DateTime.TryParse(item.GetValue(objeto, null).ToString(), out datasInicializadasNaoNulas))
                    {
                        DateTime data = DateTime.Parse(item.GetValue(objeto, null).ToString());
                        if (data < DateTime.Parse("01/01/1753") || data > DateTime.Parse("31/12/9999"))
                        {
                            continue;
                        }
                    }

                    if (like)
                    {
                        sql += " and " + item.Name + " like @" + item.Name; // item.GetValue(pessoa, null);
                    }
                    else
                    {
                        //itens booleanos a serem pulados.
                        if (item.Name == "Emergencia" || item.Name == "ConcordaServicoRealizado")
                            continue;
                        else
                            sql += " and " + item.Name + " = @" + item.Name; // item.GetValue(pessoa, null);
                    }
                }
            }

            List<T> lista = new List<T>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = sql;
                cmd.Connection = conn;

                foreach (PropertyInfo item in objeto.GetType().GetProperties())
                {
                    bool campoVirtual = false;

                    foreach (Attribute attr in item.GetCustomAttributes(true))
                    {
                        CampoVirtual campo = attr as CampoVirtual;
                        if (campo != null)
                        {
                            campoVirtual = campo.campoVirtual;
                        }
                    }

                    if (!campoVirtual && item.CanRead && item.GetValue(objeto, null) != null && !string.IsNullOrEmpty(item.GetValue(objeto, null).ToString()) && item.GetValue(objeto, null).ToString() != "0")
                    {
                        if (item.PropertyType == typeof(DateTime) && DateTime.TryParse(item.GetValue(objeto, null).ToString(), out datasInicializadasNaoNulas))
                        {
                            DateTime data = DateTime.Parse(item.GetValue(objeto, null).ToString());
                            if (data < DateTime.Parse("01/01/1753") || data > DateTime.Parse("31/12/9999"))
                            {
                                continue;
                            }
                        }

                        if (like)
                        {
                            cmd.Parameters.Add(new SqlParameter("@" + item.Name, "%" + item.GetValue(objeto, null) + "%"));
                        }
                        else
                        {
                            if (item.Name == "Emergencia") //para OrdemDeServicoBO problemas conversao 
                            {
                                continue;
                                //sql server está usando smallint para boolean então...
                                if (item.GetValue(objeto, null).ToString() == "False")
                                    cmd.Parameters.Add(new SqlParameter("@" + item.Name, Convert.ToInt16(0)));
                                else if (item.GetValue(objeto, null).ToString() == "True")
                                    cmd.Parameters.Add(new SqlParameter("@" + item.Name, Convert.ToInt16(1)));
                            }
                            else
                                cmd.Parameters.Add(new SqlParameter("@" + item.Name, item.GetValue(objeto, null)));
                        }
                    }
                }

                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    T encontrado = new T();

                    foreach (PropertyInfo item in encontrado.GetType().GetProperties())
                    {
                        bool campoVirtual = false;

                        foreach (Attribute attr in item.GetCustomAttributes(true))
                        {
                            CampoVirtual campo = attr as CampoVirtual;
                            if (campo != null)
                            {
                                campoVirtual = campo.campoVirtual;
                            }
                        }

                        if (!campoVirtual && item.CanWrite && dr[item.Name] != DBNull.Value && item.Name != "MOMALT")
                        {
                            //por causa de problemas de conversao banco SQLServer 2005 --> O objeto de tipo 'System.Int16' não pode ser convertido no tipo 'System.Boolean'.
                            if (item.PropertyType == typeof(bool))
                            {
                                item.SetValue(encontrado, Convert.ToBoolean(dr[item.Name]), null);
                            }
                            else if (item.PropertyType == typeof(System.Int32))
                            {
                                try
                                {
                                    item.SetValue(encontrado, Convert.ToInt32(dr[item.Name]), null);
                                }
                                catch (Exception)
                                {
                                    item.SetValue(encontrado, Convert.ToInt32(dr[item.Name].ToString()), null);
                                }

                            }
                            else
                            {
                                item.SetValue(encontrado, dr[item.Name], null);
                            }
                        }
                    }

                    lista.Add(encontrado);
                }
            }
            return lista;
        }

        public static T ExecSQLUm(string sql)
        {
            string connectionString = System.Configuration.ConfigurationSettings.AppSettings["conn"];

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = sql;
                cmd.Connection = conn;

                return (T)cmd.ExecuteScalar();

            }
        }

        public static List<T> ExecSQL(string sql, T objeto)
        {
            string connectionString = System.Configuration.ConfigurationSettings.AppSettings["conn"];

            List<T> lista = new List<T>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = sql;
                cmd.Connection = conn;

                if (objeto != null)
                {
                    SqlDataReader dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        T encontrado = new T();

                        foreach (PropertyInfo item in encontrado.GetType().GetProperties())
                        {
                            bool campoVirtual = false;

                            foreach (Attribute attr in item.GetCustomAttributes(true))
                            {
                                CampoVirtual campo = attr as CampoVirtual;
                                if (campo != null)
                                {
                                    campoVirtual = campo.campoVirtual;
                                }
                            }

                            if (!campoVirtual && item.CanWrite && dr[item.Name] != DBNull.Value && item.Name != "MOMALT")
                            {
                                //por causa de problemas de conversao banco SQLServer 2005 --> O objeto de tipo 'System.Int16' não pode ser convertido no tipo 'System.Boolean'.
                                if (item.PropertyType == typeof(bool))
                                {
                                    item.SetValue(encontrado, Convert.ToBoolean(dr[item.Name]), null);
                                }
                                else
                                {
                                    item.SetValue(encontrado, dr[item.Name], null);
                                }
                            }
                        }
                        lista.Add(encontrado);
                    }
                    return lista;
                }
                else
                {
                    if (cmd.ExecuteNonQuery() > 0)
                        return new List<T>();
                    else
                        return null;
                }
            }
        }

        public static List<T> Find(T objeto)
        {
            List<T> lista = new List<T>();
            return lista;
        }

        public static int Update(T objeto)
        {
            Alvo sistemaAlvo = Alvo.Orbita;

            foreach (Attribute attr in objeto.GetType().GetCustomAttributes(true))
            {
                Sistema sistema = attr as Sistema;
                if (sistema != null)
                {
                    sistemaAlvo = sistema.sistemaAlvo;
                }
            }

            string connectionString = System.Configuration.ConfigurationSettings.AppSettings["conn"];

            string prefixoTabela = string.Empty; // SistemaBO.getPrefixoTabela(sistemaAlvo);
            string tabela = objeto.GetType().Name;
            string sql = "update " + prefixoTabela + tabela + " set ";
            string sqlFields = string.Empty;

            DateTime datasInicializadasNaoNulas = new DateTime();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;

                foreach (PropertyInfo item in objeto.GetType().GetProperties())
                {
                    bool campoVirtual = false;

                    foreach (Attribute attr in item.GetCustomAttributes(true))
                    {
                        CampoVirtual campo = attr as CampoVirtual;
                        if (campo != null)
                        {
                            campoVirtual = campo.campoVirtual;
                        }
                    }

                    if (item.CanRead && !campoVirtual && item.GetValue(objeto, null) != null && !string.IsNullOrEmpty(item.GetValue(objeto, null).ToString()) && item.GetValue(objeto, null).ToString() != "0")
                    {
                        if (item.Name.ToUpper() == "ID")
                            continue;

                        if (item.Name == "CadastroAtivo")
                            continue;

                        if (item.PropertyType == typeof(DateTime) && DateTime.TryParse(item.GetValue(objeto, null).ToString(), out datasInicializadasNaoNulas))
                        {
                            DateTime data = DateTime.Parse(item.GetValue(objeto, null).ToString());
                            if (data < DateTime.Parse("01/01/1753") || data > DateTime.Parse("31/12/9999"))
                            {
                                continue;
                            }
                        }

                        if (item.Name.ToUpper() != "UID")
                        {
                            sqlFields += item.Name + " = @" + item.Name + ", ";
                        }

                        cmd.Parameters.Add(new SqlParameter("@" + item.Name, item.GetValue(objeto, null)));
                    }
                }

                sqlFields = sqlFields.Substring(0, sqlFields.Length - 2);
                sql += sqlFields + " where uid = @uid";
                cmd.CommandText = sql;

                return cmd.ExecuteNonQuery();

            }
        }

        public static int Insert(T objeto)
        {
            string connectionString = System.Configuration.ConfigurationSettings.AppSettings["conn"];

            Alvo sistemaAlvo = Alvo.Orbita;

            foreach (Attribute attr in objeto.GetType().GetCustomAttributes(true))
            {

                Sistema sistema = attr as Sistema;
                if (sistema != null)
                {
                    sistemaAlvo = sistema.sistemaAlvo;
                }
            }

            string tabela = objeto.GetType().Name;
            string prefixoTabela = string.Empty; // SistemaBO.getPrefixoTabela(sistemaAlvo);
            string sql = "insert " + prefixoTabela + tabela + "(";
            string sqlFields = string.Empty;
            string sqlParameters = string.Empty;

            DateTime datasInicializadasNaoNulas = new DateTime();

            foreach (PropertyInfo item in objeto.GetType().GetProperties())
            {
                bool campoVirtual = false;

                foreach (Attribute attr in item.GetCustomAttributes(true))
                {
                    CampoVirtual campo = attr as CampoVirtual;
                    if (campo != null)
                    {
                        campoVirtual = campo.campoVirtual;
                    }
                }

                // verificar se o campo nao é ID, se é ID pula, e deixa passar o zero, pois hj um int com valor zero não está gravando. esta null
                if (item.CanRead && !campoVirtual && item.GetValue(objeto, null) != null && !string.IsNullOrEmpty(item.GetValue(objeto, null).ToString()) && item.GetValue(objeto, null).ToString() != "0")
                {
                    if (item.PropertyType == typeof(DateTime) && DateTime.TryParse(item.GetValue(objeto, null).ToString(), out datasInicializadasNaoNulas))
                    {
                        DateTime data = DateTime.Parse(item.GetValue(objeto, null).ToString());
                        if (data < DateTime.Parse("01/01/1753") || data > DateTime.Parse("31/12/9999"))
                        {
                            continue;
                        }
                    }

                    sqlFields += item.Name + ",";
                    sqlParameters += "@" + item.Name + ",";
                }
            }

            sqlFields = sqlFields.Substring(0, sqlFields.Length - 1);
            sqlParameters = sqlParameters.Substring(0, sqlParameters.Length - 1);
            sql += sqlFields + ") values (" + sqlParameters + ")";

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = sql;
                cmd.Connection = conn;

                foreach (PropertyInfo item in objeto.GetType().GetProperties())
                {

                    bool campoVirtual = false;

                    foreach (Attribute attr in item.GetCustomAttributes(true))
                    {
                        CampoVirtual campo = attr as CampoVirtual;
                        if (campo != null)
                        {
                            campoVirtual = campo.campoVirtual;
                        }
                    }
                    if (!campoVirtual && item.CanRead && item.GetValue(objeto, null) != null && !string.IsNullOrEmpty(item.GetValue(objeto, null).ToString()) && item.GetValue(objeto, null).ToString() != "0")
                    {
                        if (item.PropertyType == typeof(DateTime) && DateTime.TryParse(item.GetValue(objeto, null).ToString(), out datasInicializadasNaoNulas))
                        {
                            DateTime data = DateTime.Parse(item.GetValue(objeto, null).ToString());
                            if (data < DateTime.Parse("01/01/1753") || data > DateTime.Parse("31/12/9999"))
                            {
                                continue;
                            }
                        }

                        cmd.Parameters.Add(new SqlParameter("@" + item.Name, item.GetValue(objeto, null)));
                    }
                }

                int z = cmd.ExecuteNonQuery();

                //após inserir, registra uma pendencia
                //if (tabela == "OrdemDeServicoDetalheTO")
                //{
                //    OrdemDeServicoDetalheTO det = objeto as OrdemDeServicoDetalheTO;
                //    if (det.AlertaPendencia)
                //    {
                //        //OrdemDeServicoBO.AlertaPendencia(det);
                //    }

                //    //enviar e-mail
                //}

                //if (tabela == "ChamadoTecnicoDetalheTO")
                //{
                //    ChamadoTecnicoDetalheTO det = objeto as ChamadoTecnicoDetalheTO;                    
                //    if (!string.IsNullOrEmpty(det.Fluxo))
                //    {                        
                //        ChamadoTecnicoBO.RegistraFluxo(det.ChamadoTecnicoUID, det.Fluxo);
                //    }
                //}

                return z;
            }
        }

        public static int Delete(T objeto)
        {
            string connectionString = System.Configuration.ConfigurationSettings.AppSettings["conn"];

            string tabela = objeto.GetType().Name;
            string sql = "delete ob_" + tabela + " where UID = @UID";

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = sql;
                cmd.Connection = conn;

                foreach (PropertyInfo item in objeto.GetType().GetProperties())
                {
                    if (item.Name.ToUpper() == "UID" && item.CanRead && item.GetValue(objeto, null) != null && !string.IsNullOrEmpty(item.GetValue(objeto, null).ToString()) && item.GetValue(objeto, null).ToString() != "0")
                    {
                        cmd.Parameters.Add(new SqlParameter("@" + item.Name.ToUpper(), item.GetValue(objeto, null)));
                    }
                }

                return cmd.ExecuteNonQuery();

            }
        }
    }
}