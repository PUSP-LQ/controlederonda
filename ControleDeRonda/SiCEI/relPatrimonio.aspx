﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiCEI/MasterSiCEI.Master" AutoEventWireup="true" CodeBehind="relPatrimonio.aspx.cs" Inherits="SistemasOnline.SiCEI.relPatrimonio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table style="width: 100%;">
        <tr>
            <td>
                &nbsp;Código:</td>
            <td>
                &nbsp;
                <asp:TextBox ID="TextBoxCodPatrimonio" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Bem Material:
            </td>
            <td>
                &nbsp;
                <asp:TextBox ID="TextBoxBemMaterial" runat="server" Height="20px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>          
            
            <asp:Panel ID="PanelAdm" runat="server">
                <table style="width: 80%;">
                    <tr>
                        <td>
                            Pessoa:
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxNomeSolic" runat="server"></asp:TextBox>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:ImageButton ID="ImageButton1" CausesValidation="false" 
                                ToolTip="Procurar Pessoa" AlternateText="Procurar Pessoa" 
                                ImageUrl="../Imagens/findPeople.png" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Encontrados:</td>
                        <td>
                            <asp:GridView ID="GridViewPessoaEncontrada" runat="server" AutoGenerateColumns="False" 
                                CellPadding="4" DataSourceID="ObjectDataSourcePessoa" ForeColor="#333333" 
                                GridLines="None"  onselectedindexchanged="GridViewPessoaEncontrada_SelectedIndexChanged" 
                                Width="100%">
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <Columns>
                                    <asp:CommandField SelectText="Selecionar" ShowSelectButton="True" />
                                    <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" />
                                    <asp:BoundField DataField="Nome" HeaderText="Nome" SortExpression="Nome" />
                                </Columns>
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                            </asp:GridView>
                            <asp:ObjectDataSource ID="ObjectDataSourcePessoa" runat="server" 
                                OldValuesParameterFormatString="original_{0}" SelectMethod="getPessoaByNome" 
                                TypeName="CamadaSQLServer.PessoaDAO">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="TextBoxNomeSolic" Name="nomePessoa" 
                                        PropertyName="Text" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Selecionado:</td>
                        <td>
                            <asp:Label ID="LabelId" runat="server" Text=""></asp:Label>
                            &nbsp;<asp:Label ID="LabelNome" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

<table style="width: 88%;" runat="server" id="tblUnidade"><tr><td>Unidade </td><td>
                
                <asp:DropDownList ID="DropDownListUnidade" runat="server" DataSourceID="ObjectDataSourceUnidade"
                                    DataTextField="descricao" DataValueField="codigo" 
                    AutoPostBack="True"></asp:DropDownList>
                    
                    <asp:ObjectDataSource ID="ObjectDataSourceUnidade" runat="server" OldValuesParameterFormatString="original_{0}"
                                    SelectMethod="GetData" TypeName="SistemasOnline.App_Data.dsDvCampusTableAdapters.unidadeTableAdapter">
                    </asp:ObjectDataSource> 
                                       
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server"><ProgressTemplate>
                        <img src="/Imagens/ajax-loader.gif" /></ProgressTemplate>
                    </asp:UpdateProgress>
                    
</td></tr>
                    <tr><td>Seção: </td><td>
                    
                    <asp:DropDownList ID="DropDownListSecao" runat="server" DataSourceID="ObjectDataSourceSecao"
                                    DataTextField="descricao" DataValueField="codigo" EnableViewState="false"></asp:DropDownList>
                        
                    <asp:ObjectDataSource ID="ObjectDataSourceSecao" runat="server" OldValuesParameterFormatString="original_{0}"
                                    SelectMethod="GetData" TypeName="SistemasOnline.App_Data.dsDvCampusTableAdapters.secaoTableAdapter">
                                    <SelectParameters><asp:ControlParameter ControlID="DropDownListUnidade" Name="superior" PropertyName="SelectedValue"
                                            Type="String" /></SelectParameters></asp:ObjectDataSource>
                                            
                                            </td></tr></table></ContentTemplate></asp:UpdatePanel>

            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="ButtonBuscar" runat="server" Text="Buscar" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Total encontrados:</td>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
    </table>

    <asp:GridView ID="GridView1" runat="server" DataSourceID="ObjectDataSource1" 
        AutoGenerateColumns="False">
        <Columns>
            <asp:CommandField SelectText="Histórico" ShowSelectButton="True" />
            <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" />
            <asp:BoundField DataField="Codigo" HeaderText="Código" 
                SortExpression="Codigo" />
            <asp:BoundField DataField="BemMaterial" HeaderText="Bem/Material" 
                SortExpression="BemMaterial" />
            <asp:BoundField DataField="UIDPessoa" HeaderText="Pessoa" 
                SortExpression="UIDPessoa" />
            <asp:BoundField DataField="CodSecao" HeaderText="Seção" 
                SortExpression="CodSecao" />
            <asp:BoundField DataField="Local" HeaderText="Local" SortExpression="Local" />
            <asp:BoundField DataField="cadmom" HeaderText="Mom." 
                SortExpression="cadmom" />
        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="get" 
        TypeName="CamadaSQLServer.SiCEI.Fisica.PatrimonioDAO">
        <SelectParameters>
            <asp:ControlParameter ControlID="TextBoxCodPatrimonio" Name="codpatrimonio" 
                PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="TextBoxBemMaterial" Name="bemmaterial" 
                PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="LabelId" Name="pessoaid" PropertyName="Text" 
                Type="String" />
            <asp:ControlParameter ControlID="DropDownListSecao" Name="codsecao" 
                PropertyName="SelectedValue" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
