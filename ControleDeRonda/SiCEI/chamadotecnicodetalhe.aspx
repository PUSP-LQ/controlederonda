<%@ Page Title="" Language="C#" MasterPageFile="~/SiCEI/MasterSiCEI.Master" AutoEventWireup="true" CodeBehind="chamadotecnicodetalhe.aspx.cs" Inherits="SistemasOnline.SiCEI.chamadotecnicodetalhe" EnableViewState="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width:90%;">
        <tr>
            <td>
                Solicitante:</td>
            <td>
                <asp:Label ID="LabelSolicitante" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Servi�o:</td>
            <td>
                <asp:Label ID="LabelServico" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Patrimonio:</td>
            <td>
                <asp:Label ID="LabelPatrimonio" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Se��o:</td>
            <td>
                <asp:Label ID="LabelSecao" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Assunto:</td>
            <td>
                <asp:Label ID="LabelAssunto" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                C�digo:</td>
            <td>
                <asp:Label ID="LabelCodigo" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Atendente:</td>
            <td>
                <asp:Label ID="LabelAtendente" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="LabelUIDChamado" runat="server" Text=""></asp:Label></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    <br />
    <br />
    <asp:GridView ID="GridView1" runat="server" Width="90%" 
        AutoGenerateColumns="False" DataSourceID="ObjectDataSource1" EnableViewState="false">
        <Columns>
            <asp:BoundField DataField="contador" HeaderText="" 
                SortExpression="contador" />
            <asp:BoundField DataField="interacao" HeaderText="Intera��o" 
                SortExpression="interacao" />
            <asp:BoundField DataField="momento" HeaderText="Momento" 
                SortExpression="momento" />
            <asp:BoundField DataField="autor" HeaderText="Autor" 
                SortExpression="autor" />
        </Columns>
    </asp:GridView>  
    
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="getDetalhe" 
        TypeName="CamadaSQLServer.SiCEI.Fisica.ChamadoTecnicoDAO">
        <SelectParameters>
            <asp:ControlParameter ControlID="LabelUIDChamado" Name="uidchamado" PropertyName="Text" 
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <br />
    
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="1">
    <asp:View ID="View3" runat="server">
     Receber 
        <asp:Button ID="Button2" runat="server" Text="Receber Chamado para Atendimento" />
    </asp:View>
        <asp:View ID="View1" runat="server">
    <asp:Panel ID="PanelInteracao" runat="server">
        <br />
        <table style="width:100%;">
            <tr>
                <td>
                    Intera��o:
                </td>
                <td>
                    <asp:CheckBox ID="CheckBoxRisca" runat="server" Text="Riscar �ltima intera��o" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:TextBox ID="TextBoxInteracao" runat="server" Columns="80" Rows="7" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <br />                    
                    Encaminhar:
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <br />
                </td>
                <td>
                    <br />
                    &nbsp;&nbsp;
                <asp:DropDownList ID="DropDownListAtendente" runat="server" 
                    DataSourceID="ObjectDataSourceAtendente" DataTextField="Apresentacao" 
                    DataValueField="UID" AppendDataBoundItems="true">
                    <asp:ListItem></asp:ListItem>
                </asp:DropDownList>
                <asp:ObjectDataSource ID="ObjectDataSourceAtendente" runat="server" 
                    OldValuesParameterFormatString="original_{0}" SelectMethod="getAtendentes" 
                    TypeName="CamadaSQLServer.PessoaDAO">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="SICEI" Name="atende" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                    <br />
                </td>
            </tr>
            <tr>
                <td align="center">
                    &nbsp;</td>
                <td>
                    <asp:Label ID="LabelMsg" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="ButtonSalvar" runat="server" onclick="ButtonSalvar_Click" 
                        Text="Salvar" />
                </td>
                <td align="center">
                    <asp:Button ID="ButtonSalvarFechar" runat="server" 
                        onclick="ButtonSalvarFechar_Click" Text="Salvar e Fechar" />
                </td>
            </tr>
        </table>
    <br />
        <br />
    </asp:Panel>
        </asp:View>
    <asp:View ID="View4" runat="server">
     Reabrir chamado:
        <asp:Button ID="Button3" runat="server" Text="Reabrir" />
    </asp:View>        
        <asp:View ID="View2" runat="server">
            Tempo do atendimento:
        <br />
        <br />
            <asp:RadioButtonList ID="RadioButtonListTempo" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem>Lento</asp:ListItem>
                <asp:ListItem>Normal</asp:ListItem>
                <asp:ListItem>R�pido</asp:ListItem>
                <asp:ListItem>N�o avaliado</asp:ListItem>
            </asp:RadioButtonList>  
        <br />
        <br />
        Qualidade da solu��o apresentada:
            <asp:RadioButtonList ID="RadioButtonListQualidade" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem>Ruim</asp:ListItem>
                <asp:ListItem>Bom</asp:ListItem>
                <asp:ListItem>�timo</asp:ListItem>
                <asp:ListItem>N�o avaliado</asp:ListItem>
            </asp:RadioButtonList>  
        <br />
        <br />
          Coment�rio / Sugest�o / Critica: 
          <br />
          <br />
            <asp:TextBox ID="TextBoxComentario" runat="server" TextMode="MultiLine" Rows="5" Columns="50"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="ButtonSalvarQualidade" runat="server" Text="Salvar" 
                onclick="ButtonSalvarQualidade_Click" />
        </asp:View>
        <asp:View ID="View5" runat="server">
            Salvo com Sucesso. Muito obrigado por sua colabora��o.
        </asp:View>
        <asp:View ID="View6" runat="server">
            <asp:Label ID="Label1" runat="server" Text="J� foi avaliado. Obrigado."></asp:Label>
        </asp:View>        
    </asp:MultiView>     
    
    </asp:Content>
