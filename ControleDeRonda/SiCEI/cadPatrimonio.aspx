﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiCEI/MasterSiCEI.Master" AutoEventWireup="true" CodeBehind="cadPatrimonio.aspx.cs" Inherits="SistemasOnline.SiCEI.cadPatrimonio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table style="width:80%;">
        <tr>
            <td colspan="2">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <table style="width:100%;">
                            <tr>
                                <td>
                                    Patrimônio:
                                    <asp:TextBox ID="TextBoxPatrimonio" runat="server" ValidationGroup="buscar"></asp:TextBox>
                                    &nbsp;<asp:Button ID="ButtonBuscarPatrimonio" runat="server" Text="Buscar" 
                                        onclick="ButtonBuscarPatrimonio_Click" ValidationGroup="buscar" />
                                    <br />
                                    <!-- se encontra eu seto os controles e disable<br /-->
                                    <asp:Label ID="LabelEncontrado" runat="server"></asp:Label>
                                    <br />
                                </td>
                            </tr>                        
                            <tr>
                                <td>
                                    Material:</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButtonList ID="RadioButtonListMaterial" runat="server" RepeatColumns="3" 
                                        AutoPostBack="True" 
                                        onselectedindexchanged="RadioButtonList1_SelectedIndexChanged">
                                        <asp:ListItem Value="Monitor">Monitor</asp:ListItem>
                                        <asp:ListItem Value="Microcomputador">Microcomputador</asp:ListItem>
                                        <asp:ListItem Value="Impressora">Impressora</asp:ListItem>
                                        <asp:ListItem Value="Nobreak">No-Break</asp:ListItem>
                                        <asp:ListItem Value="Scanner">Scanner</asp:ListItem>
                                        <asp:ListItem Value="Switch">Switch</asp:ListItem>
                                        <asp:ListItem Value="Acesspoint">Acess Point</asp:ListItem>
                                        <asp:ListItem Value="Antena">Antena</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:MultiView ID="MultiView1" runat="server">
                                        <asp:View ID="ViewMonitor" runat="server">
                                            <table style="width:100%;">
                                                <tr>
                                                    <td>
                                                        Marca</td>
                                                    <td>
                                                        <asp:TextBox ID="TextBoxMonitorMarca" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Polegada</td>
                                                    <td>
                                                        <asp:TextBox ID="TextBoxMonitorPolegada" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Wide</td>
                                                    <td>
                                                        <asp:CheckBox ID="CheckBoxMonitoWide" runat="server" Checked="true" />
                                                    </td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                        <asp:View ID="ViewMicrocomputador" runat="server">
                                            <table style="width:100%;">
                                                <tr>
                                                    <td>
                                                        Marca:</td>
                                                    <td>
                                                        <asp:TextBox ID="TextBoxMicroComputadorMarca" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Sistema Operacional:</td>
                                                    <td>
                                                        <asp:RadioButtonList ID="RadioButtonListMicroComputadorSistemaOperacional" runat="server" RepeatColumns="2">
                                                            <asp:ListItem>XP</asp:ListItem>
                                                            <asp:ListItem>VISTA</asp:ListItem>
                                                            <asp:ListItem>7</asp:ListItem>
                                                            <asp:ListItem>Outros</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Office:</td>
                                                    <td>
                                                        <asp:RadioButtonList ID="RadioButtonListMicroComputadorOffice" 
                                                            runat="server" RepeatColumns="2">
                                                            <asp:ListItem>XP</asp:ListItem>
                                                            <asp:ListItem>VISTA</asp:ListItem>
                                                            <asp:ListItem>7</asp:ListItem>
                                                            <asp:ListItem>Outros</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Modelo:</td>
                                                    <td>
                                                        <asp:TextBox ID="TextBoxMicroComputadorModelo" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Processador:</td>
                                                    <td>
                                                        <asp:TextBox ID="TextBoxMicroComputadorProcessador" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Mem. Ram</td>
                                                    <td>
                                                        <asp:TextBox ID="TextBoxMicroComputadorMemRam" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        HD</td>
                                                    <td>
                                                        <asp:TextBox ID="TextBoxMicroComputadorHD" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Rede sem fio</td>
                                                    <td>
                                                        <asp:CheckBox ID="CheckBoxMicroComputadorRedeSemFio" runat="server" />
                                                    </td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        IP</td>
                                                    <td>
                                                        <asp:TextBox ID="TextBoxMicroComputadorIP" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Backup:</td>
                                                    <td>
                                                        <asp:CheckBox ID="CheckBoxMicroComputadorBackup" runat="server" />
                                                    </td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                        <asp:View ID="ViewImpressora" runat="server">
                                            <asp:CheckBoxList ID="CheckBoxListImpressora" runat="server" RepeatColumns="3">
                                                <asp:ListItem>Tinta</asp:ListItem>
                                                <asp:ListItem>Laser</asp:ListItem>
                                                <asp:ListItem>Colorida</asp:ListItem>
                                            </asp:CheckBoxList>
                                        </asp:View>
                                        <asp:View ID="ViewNoBreak" runat="server">
                                        </asp:View> 
                                        <asp:View ID="ViewScanner" runat="server">
                                        </asp:View>                                        
                                        <asp:View ID="ViewSwitch" runat="server">
                                        </asp:View>                                                                                
                                        <asp:View ID="ViewAccessPoint" runat="server">
                                        </asp:View> 
                                        <asp:View ID="ViewAntena" runat="server">
                                        </asp:View>                                                                                                                                                                                                                                              
                                    </asp:MultiView>
                                </td>
                            </tr>
                            <tr>
                            <td>
                                <table style="width: 100%;">
                                    <tr>
                                        <td>
                                            Ação:</td>
                                        <td>
                                            &nbsp;
                                            <asp:RadioButtonList ID="RadioButtonListAcao" runat="server" 
                                                onselectedindexchanged="RadioButtonListAcao_SelectedIndexChanged" 
                                                AutoPostBack="True">
                                                <asp:ListItem Selected="True" Value="registro">Registro</asp:ListItem>
                                                <asp:ListItem Value="baixa">Baixa Patrimonial (CEDIR)</asp:ListItem>
                                                <asp:ListItem Value="saidacce">Saída Conserto CCE</asp:ListItem>
                                                <asp:ListItem Value="saidagarforn">Saída Garantia Fornecedor</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>

                                    </tr>
                                </table>
                            </tr>                            
                            <tr>
                            <td>
                                <asp:MultiView ID="MultiView2" runat="server" ActiveViewIndex="0">
                                        <asp:View ID="View1" runat="server">
                                        

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>          
            
            <asp:Panel ID="PanelAdm" runat="server">
                <table style="width: 80%;">
                    <tr>
                        <td>
                            Solicitante:
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxNomeSolic" runat="server"></asp:TextBox>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:ImageButton ID="ImageButton1" CausesValidation="false" 
                                ToolTip="Procurar Pessoa" AlternateText="Procurar Pessoa" 
                                ImageUrl="../Imagens/findPeople.png" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Encontrados:</td>
                        <td>
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                                CellPadding="4" DataSourceID="ObjectDataSourcePessoa" ForeColor="#333333" 
                                GridLines="None" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" 
                                Width="100%">
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <Columns>
                                    <asp:CommandField SelectText="Selecionar" ShowSelectButton="True" />
                                    <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" />
                                    <asp:BoundField DataField="Nome" HeaderText="Nome" SortExpression="Nome" />
                                </Columns>
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                            </asp:GridView>
                            <asp:ObjectDataSource ID="ObjectDataSourcePessoa" runat="server" 
                                OldValuesParameterFormatString="original_{0}" SelectMethod="getPessoaByNome" 
                                TypeName="CamadaSQLServer.PessoaDAO">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="TextBoxNomeSolic" Name="nomePessoa" 
                                        PropertyName="Text" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Selecionado:</td>
                        <td>
                            <asp:Label ID="LabelId" runat="server" Text=""></asp:Label>
                            &nbsp;<asp:Label ID="LabelNome" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

<table style="width: 88%;" runat="server" id="tblUnidade"><tr><td>Unidade </td><td>
                
                <asp:DropDownList ID="DropDownListUnidade" runat="server" DataSourceID="ObjectDataSourceUnidade"
                                    DataTextField="descricao" DataValueField="codigo" 
                    AutoPostBack="True" ondatabound="DropDownListUnidade_DataBound"></asp:DropDownList>
                    
                    <asp:ObjectDataSource ID="ObjectDataSourceUnidade" runat="server" OldValuesParameterFormatString="original_{0}"
                                    SelectMethod="GetData" TypeName="SistemasOnline.App_Data.dsDvCampusTableAdapters.unidadeTableAdapter">
                    </asp:ObjectDataSource> 
                                       
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server"><ProgressTemplate>
                        <img src="/Imagens/ajax-loader.gif" /></ProgressTemplate>
                    </asp:UpdateProgress>
                    
</td></tr>
                    <tr><td>Seção: </td><td>
                    
                    <asp:DropDownList ID="DropDownListSecao" runat="server" DataSourceID="ObjectDataSourceSecao"
                                    DataTextField="descricao" DataValueField="codigo" EnableViewState="false"
                        ondatabound="DropDownListSecao_DataBound"></asp:DropDownList>
                        
                    <asp:ObjectDataSource ID="ObjectDataSourceSecao" runat="server" OldValuesParameterFormatString="original_{0}"
                                    SelectMethod="GetData" TypeName="SistemasOnline.App_Data.dsDvCampusTableAdapters.secaoTableAdapter">
                                    <SelectParameters><asp:ControlParameter ControlID="DropDownListUnidade" Name="superior" PropertyName="SelectedValue"
                                            Type="String" /></SelectParameters></asp:ObjectDataSource>
                                            
                                            </td></tr><tr><td>
                    Local:</td><td><asp:TextBox ID="TextBoxLocal" runat="server"></asp:TextBox>
                        &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidatorLocal" runat="server" 
                            ControlToValidate="TextBoxLocal" Display="Dynamic" ErrorMessage="(*)"></asp:RequiredFieldValidator>
                    </td></tr></table></ContentTemplate></asp:UpdatePanel>


                                        </asp:View>   
                                          <asp:View ID="View3" runat="server"></asp:View>
                                </asp:MultiView>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>

        <tr>
            <td>
                Observação / Anotação:<br />
                * Dados do fornecedor do serviço<br />
                em caso de saído da campus.</td>
            <td>
                                                         <asp:TextBox ID="TextBoxObservacao" 
                    runat="server" Rows="5" TextMode="MultiLine"></asp:TextBox> 
                </td>

        </tr>

        <tr>
            <td>
                <asp:Label ID="LabelMsg" runat="server" Text=""></asp:Label>
            </td>
            <td>
                </td>

        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>

        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="ButtonSalvar" runat="server" Text="Salvar" 
                    onclick="ButtonSalvar_Click" />
            </td>

        </tr>
    </table>
</asp:Content>
