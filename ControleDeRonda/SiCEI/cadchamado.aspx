﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiCEI/MasterSiCEI.Master" AutoEventWireup="true" CodeBehind="cadchamado.aspx.cs" Inherits="SistemasOnline.SiCEI.cadchamado" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
    <script>
    <%= DropDownListAtendente.ClientID %>.SelectedValue = "1"; 
    </script>    
<asp:MultiView ID="MultiViewPrincipal" runat="server" ActiveViewIndex="0">            
<asp:View ID="ViewCadastro" runat="server">

    <table style="width:90%;">
        <tr>
            <td>                
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>  Solicitante :                                    

        </td>
<td>                                       
                <asp:TextBox ID="TextBoxNomeSolic" runat="server"></asp:TextBox>
                <cc1:AutoCompleteExtender ID="TextBoxNomeSolic_AutoCompleteExtender" 
                    runat="server" Enabled="True" 
                    ServicePath="../AutoComplete.asmx" TargetControlID="TextBoxNomeSolic" 
                    UseContextKey="True" CompletionSetCount="12" MinimumPrefixLength="1" ServiceMethod="GetCompletionList">
                </cc1:AutoCompleteExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBoxNomeSolic"
                    ErrorMessage="(*)"></asp:RequiredFieldValidator>
                &nbsp;&nbsp;&nbsp;&nbsp; <a href="CadUsuario.aspx" target="_blank"> cadastrar nova pessoa </a></td>        
        </tr>
        
        <tr>
            <td colspan="2">
                <br />
                Serviços: 
                <asp:Label ID="LabelQtdServicos" runat="server" Text="" Font-Bold="true"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:RadioButtonList ID="RadioButtonListTipoChamado" runat="server" 
                    RepeatColumns="3">
                    <asp:ListItem Value="administrativo">Administrativo - Cotação / Compras / Adiant</asp:ListItem>
                    <asp:ListItem Value="backup">Backup</asp:ListItem>
                    <asp:ListItem Value="duvidatreinamento">Dúvida - Treinamento - HelpDesk</asp:ListItem>
                    <asp:ListItem Value="email">E-mail</asp:ListItem>
                    <asp:ListItem Value="garantia">Garantia</asp:ListItem>
                    <asp:ListItem Value="impressora">Impressora - Periféricos</asp:ListItem>
                    <asp:ListItem Value="instalwin">Instalação Windows</asp:ListItem>
                    <asp:ListItem Value="manutencao">Manutenção</asp:ListItem>
                    <asp:ListItem Value="office">Office</asp:ListItem>
                    <asp:ListItem Value="programacaosistema">Programação / Sistema</asp:ListItem>
                    <asp:ListItem Value="rede">Rede</asp:ListItem>
                    <asp:ListItem Value="site">Site</asp:ListItem>
                    <asp:ListItem Value="virusantivirus">Vírus / Anti-Virus</asp:ListItem>
                </asp:RadioButtonList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ErrorMessage="(*)" ControlToValidate="RadioButtonListTipoChamado"></asp:RequiredFieldValidator>
                <br /><br />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:CheckBox ID="CheckBoxNotificacao" runat="server" Text="Notificação por e-mail" Checked="true" />
            </td>
        </tr>
        <tr>
            <td>
                Patrimônio:</td>
            <td>
                <asp:TextBox ID="TextBoxPatrimonio" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Assunto:</td>
            <td>
                <asp:TextBox ID="TextBoxAssunto" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ErrorMessage="(*)" ControlToValidate="TextBoxAssunto"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Descrição:</td>
            <td>               
                <asp:TextBox ID="TextBoxDescricao" runat="server" TextMode="MultiLine" Columns="60" Rows="5"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ErrorMessage="(*)" ControlToValidate="TextBoxDescricao"></asp:RequiredFieldValidator>                
            </td>
        </tr>
        <tr>
            <td>
                Encaminhar:&nbsp;&nbsp; </td>
            <td>
                <asp:DropDownList ID="DropDownListAtendente" runat="server" 
                    DataSourceID="ObjectDataSourceAtendente" DataTextField="Apresentacao" 
                    DataValueField="UID" AppendDataBoundItems="true" 
                    onprerender="DropDownListAtendente_PreRender">
                    <asp:ListItem></asp:ListItem>
                </asp:DropDownList>
                <asp:ObjectDataSource ID="ObjectDataSourceAtendente" runat="server" 
                    OldValuesParameterFormatString="original_{0}" SelectMethod="getAtendentes" 
                    TypeName="CamadaSQLServer.PessoaDAO">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="SICEI" Name="atende" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="ButtonSalvar" runat="server" onclick="ButtonSalvar_Click" 
                    Text="Salvar" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:View>
<asp:View ID="ViewSucesso" runat="server">
Cadastro com sucesso.

<a href="cadchamado.aspx">Nova solicitação clique aqui</a>

</asp:View>
</asp:MultiView>
    
                <br />Últimos cadastrados
    <asp:GridView ID="GridView2" runat="server" Width="90%" 
    AutoGenerateColumns="False" DataSourceID="ObjectDataSource1" EnableViewState="false">
        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" />
            <asp:BoundField DataField="Codigo" HeaderText="Codigo" 
                SortExpression="Codigo" />
            <asp:BoundField DataField="NomeSolicitante" HeaderText="NomeSolicitante" 
                SortExpression="NomeSolicitante" />
            <asp:BoundField DataField="SiglaSecao" HeaderText="SiglaSecao" 
                SortExpression="SiglaSecao" />
            <asp:BoundField DataField="Assunto" HeaderText="Assunto" 
                SortExpression="Assunto" />
            <asp:BoundField DataField="cadmom" HeaderText="cadmom" 
                SortExpression="cadmom" />
        </Columns>
    </asp:GridView>
<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
    OldValuesParameterFormatString="original_{0}" SelectMethod="get" 
    TypeName="CamadaSQLServer.SiCEI.Fisica.ChamadoTecnicoDAO">
    <SelectParameters>
        <asp:Parameter DefaultValue="10" Name="qtd" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>
</asp:Content>
