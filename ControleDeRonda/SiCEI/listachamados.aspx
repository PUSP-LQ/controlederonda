﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiCEI/MasterSiCEI.Master" AutoEventWireup="true" CodeBehind="listachamados.aspx.cs" Inherits="SistemasOnline.SiCEI.listachamados" %>
<%@ Register assembly="DevExpress.Web.v9.3, Version=9.3.2.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxNavBar" tagprefix="dx" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    Acesso Rápido: <a href="cadchamado.aspx">Novo Chamado</a>
    <br />
    <table style="width:90%;">
        <tr>
            <td>
                                Código:</td>
            <td>
                                <asp:TextBox ID="TextBoxCodigo" runat="server"></asp:TextBox>
                            </td>
            <td>
                Atendente:</td>
            <td>
                <asp:DropDownList ID="DropDownListAtendente" runat="server" 
                    DataSourceID="ObjectDataSourceAtendente" DataTextField="Apresentacao" 
                    DataValueField="UID" AutoPostBack="true" 
                      onprerender="DropDownListAtendente_PreRender">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                                Solicitante:
                            </td>
            <td>
                                <asp:TextBox ID="TextBoxNomeSolic" runat="server"></asp:TextBox>
                                <cc1:AutoCompleteExtender ID="TextBoxNomeSolic_AutoCompleteExtender" 
                    runat="server" Enabled="True" 
                    ServicePath="../AutoComplete.asmx" TargetControlID="TextBoxNomeSolic" 
                    UseContextKey="True" CompletionSetCount="12" MinimumPrefixLength="1" ServiceMethod="GetCompletionList">
                </cc1:AutoCompleteExtender>
            </td>
            <td>
                Status:</td>
            <td>
                <asp:TextBox ID="TextBox1" runat="server"  style="width: 120px;"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>              
                Data Início:</td>
            <td>
                <asp:TextBox ID="TextBoxDtInicio" runat="server" style="width: 120px;"></asp:TextBox>
            </td>
            <td>              
                Data Fim:</td>            
            <td>
                <asp:TextBox ID="TextBoxdtFim" runat="server"  style="width: 120px;"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Assunto:</td>
            <td>
                <asp:TextBox ID="TextBoxAssunto" runat="server"  style="width: 120px;"></asp:TextBox>
            </td>
            <td>
                Patrimônio:</td>
            <td>
                <asp:TextBox ID="TextBoxPatrimonio" runat="server"  style="width: 120px;"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
                <asp:Button ID="ButtonLocalizar" runat="server" Text="Localizar" 
                    onclick="ButtonLocalizar_Click" />
            </td>
        </tr>
        <!-- tr>
            <td>
                Descrição:</td>
            <td>
                <asp:TextBox ID="TextBoxDescricao" runat="server"></asp:TextBox>
            </td>
        </tr -->
        </table>
    <asp:ObjectDataSource ID="ObjectDataSourceAtendente" runat="server" 
                    OldValuesParameterFormatString="original_{0}" SelectMethod="getAtendentes" 
                    TypeName="CamadaSQLServer.PessoaDAO">
        <SelectParameters>
            <asp:Parameter DefaultValue="SICEI" Name="atende" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <br />
    Novos:
    <br />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        DataSourceID="ObjectDataSource1" 
        onselectedindexchanged="GridView1_SelectedIndexChanged">
        <Columns>
            <asp:CommandField ShowSelectButton="True" SelectText="Sel." />
            <asp:BoundField DataField="Codigo" HeaderText="Codigo" 
                SortExpression="Codigo" />
            <asp:BoundField DataField="NomeSolicitante" HeaderText="Solicitante" 
                SortExpression="NomeSolicitante" />
            <asp:BoundField DataField="SiglaSecao" HeaderText="Seção" 
                SortExpression="SiglaSecao" />
            <asp:BoundField DataField="Assunto" HeaderText="Assunto" 
                SortExpression="Assunto" />             
            <asp:BoundField DataField="cadmom" HeaderText="Momento" DataFormatString="{0:dd/MM HH:mm}"
                SortExpression="cadmom"  />                                
            <asp:BoundField DataField="AtendenteNome" HeaderText="Atendente"
                SortExpression="AtendenteNome" />                  
        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="getList" 
        TypeName="CamadaSQLServer.SiCEI.Fisica.ChamadoTecnicoDAO">
        <SelectParameters>
            <asp:Parameter Name="status" Type="String" DefaultValue="NOVO" />
            <asp:Parameter Name="qtd" Type="Int32" DefaultValue="10" />
            <asp:ControlParameter ControlID="TextBoxCodigo" DefaultValue="" Name="codigo" 
                PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="TextBoxNomeSolic" Name="solicitanteID" 
                PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="TextBoxDtInicio" Name="dtIni" 
                PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="TextBoxdtFim" Name="dtFim" PropertyName="Text" 
                Type="String" />
            <asp:ControlParameter ControlID="DropDownListAtendente" Name="atendente" 
                PropertyName="SelectedValue" Type="String" />
            <asp:ControlParameter ControlID="TextBoxPatrimonio" Name="patrimonio" 
                PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="TextBoxAssunto" Name="assunto" 
                PropertyName="Text" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <br />
    Últimos finalizados:
    <br />
    <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" 
        DataSourceID="ObjectDataSource2" 
        onselectedindexchanged="GridView3_SelectedIndexChanged">
        <Columns>
            <asp:CommandField ShowSelectButton="True" SelectText="Sel." />
            <asp:BoundField DataField="Codigo" HeaderText="Codigo" 
                SortExpression="Codigo" />
            <asp:BoundField DataField="NomeSolicitante" HeaderText="Solicitante" 
                SortExpression="NomeSolicitante" />
            <asp:BoundField DataField="SiglaSecao" HeaderText="Seção" 
                SortExpression="SiglaSecao" />
            <asp:BoundField DataField="Assunto" HeaderText="Assunto" 
                SortExpression="Assunto" />             
            <asp:BoundField DataField="cadmom" HeaderText="Momento" DataFormatString="{0:dd/MM HH:mm}"
                SortExpression="cadmom"  />                                
            <asp:BoundField DataField="AtendenteNome" HeaderText="Atendente"
                SortExpression="AtendenteNome" />                  
        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="getList" 
        TypeName="CamadaSQLServer.SiCEI.Fisica.ChamadoTecnicoDAO">
        <SelectParameters>
            <asp:Parameter Name="status" Type="String" DefaultValue="FINALIZADO" />
            <asp:Parameter Name="qtd" Type="Int32" DefaultValue="10" />   
            <asp:ControlParameter ControlID="TextBoxCodigo" DefaultValue="" Name="codigo" 
                PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="TextBoxNomeSolic" Name="solicitanteID" 
                PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="TextBoxDtInicio" Name="dtIni" 
                PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="TextBoxdtFim" Name="dtFim" PropertyName="Text" 
                Type="String" />
            <asp:ControlParameter ControlID="DropDownListAtendente" Name="atendente" 
                PropertyName="SelectedValue" Type="String" />
            <asp:ControlParameter ControlID="TextBoxPatrimonio" Name="patrimonio" 
                PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="TextBoxAssunto" Name="assunto" 
                PropertyName="Text" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>    
</asp:Content>
