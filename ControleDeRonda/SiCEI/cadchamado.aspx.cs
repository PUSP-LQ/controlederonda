﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CamadaSQLServer.Orbita;
using CamadaSQLServer.SiCEI;
using System.IO;
using System.Text;

namespace SistemasOnline.SiCEI
{
    public partial class cadchamado : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ChamadoTecnicoDAO ct = new ChamadoTecnicoDAO();

            RadioButtonListTipoChamado.Items.FindByValue("administrativo").Text = RadioButtonListTipoChamado.Items.FindByValue("administrativo").Text + " (" + ct.getQtdServico("administrativo") + ")";
            RadioButtonListTipoChamado.Items.FindByValue("backup").Text = RadioButtonListTipoChamado.Items.FindByValue("backup").Text + " (" + ct.getQtdServico("backup") + ")";
            RadioButtonListTipoChamado.Items.FindByValue("duvidatreinamento").Text = RadioButtonListTipoChamado.Items.FindByValue("duvidatreinamento").Text + " (" + ct.getQtdServico("duvidatreinamento") + ")";
            RadioButtonListTipoChamado.Items.FindByValue("email").Text = RadioButtonListTipoChamado.Items.FindByValue("email").Text + " (" + ct.getQtdServico("email") + ")";
            RadioButtonListTipoChamado.Items.FindByValue("garantia").Text = RadioButtonListTipoChamado.Items.FindByValue("garantia").Text + " (" + ct.getQtdServico("garantia") + ")";
            RadioButtonListTipoChamado.Items.FindByValue("impressora").Text = RadioButtonListTipoChamado.Items.FindByValue("impressora").Text + " (" + ct.getQtdServico("impressora") + ")";
            RadioButtonListTipoChamado.Items.FindByValue("manutencao").Text = RadioButtonListTipoChamado.Items.FindByValue("manutencao").Text + " (" + ct.getQtdServico("manutencao") + ")";
            RadioButtonListTipoChamado.Items.FindByValue("instalwin").Text = RadioButtonListTipoChamado.Items.FindByValue("instalwin").Text + " (" + ct.getQtdServico("instalwin") + ")";
            RadioButtonListTipoChamado.Items.FindByValue("programacaosistema").Text = RadioButtonListTipoChamado.Items.FindByValue("programacaosistema").Text + " (" + ct.getQtdServico("programacaosistema") + ")";
            RadioButtonListTipoChamado.Items.FindByValue("rede").Text = RadioButtonListTipoChamado.Items.FindByValue("rede").Text + " (" + ct.getQtdServico("rede") + ")";
            RadioButtonListTipoChamado.Items.FindByValue("site").Text = RadioButtonListTipoChamado.Items.FindByValue("site").Text + " (" + ct.getQtdServico("site") + ")";
            RadioButtonListTipoChamado.Items.FindByValue("virusantivirus").Text = RadioButtonListTipoChamado.Items.FindByValue("virusantivirus").Text + " (" + ct.getQtdServico("virusantivirus") + ")";

            LabelQtdServicos.Text = ct.getQtdServico(string.Empty) + " serviços na fila.";

        }

        protected void ButtonSalvar_Click(object sender, EventArgs e)
        {
            ChamadoTecnicoTO ct = new ChamadoTecnicoTO();
            ct.Assunto = TextBoxAssunto.Text;
            ct.Local = "";

            string id = TextBoxNomeSolic.Text.Substring(0, TextBoxNomeSolic.Text.IndexOf("-"));

            if (string.IsNullOrEmpty(id))
            {
                return;
            }

            ct.Solicitante = PessoaBO.getPessoaFromID(Convert.ToInt32(id));
            ct.Secao = new DVCampusDAO().getSecao(ct.Solicitante.Secao);
            
            ct.TipoChamado = new TipoChamadoTO(RadioButtonListTipoChamado.SelectedValue);
            ct.UserCreate = User.Identity.Name;
            ct.xHost = Request.UserHostAddress;
            ct.Atendente = PessoaBO.getPessoaFromUID(DropDownListAtendente.SelectedValue);
            ct.Patrimonio = TextBoxPatrimonio.Text;
            
            ChamadoTecnicoDetalheTO det = new ChamadoTecnicoDetalheTO();
            det.autor = PessoaBO.getPessoaFromUserName(User.Identity.Name);
            det.chamado = ct.UID;
            det.contador = 1;
            det.etapa = 1;
            det.interacao = TextBoxDescricao.Text;

            ct.Detalhes.Add(det);

            new ChamadoTecnicoDAO().Salvar(ct);

            MultiViewPrincipal.ActiveViewIndex = 1;
            
            //enviar e-mail
            if (CheckBoxNotificacao.Checked)
            {
                #region enviaemail
                StreamReader objReader;

                //envio para o solicitante
                try
                {
                    objReader = new StreamReader(Request.PhysicalApplicationPath + @"\Template\Email\AvisoChamadoTecnico.html");
                }
                catch (Exception ex01)
                {
                    //LabelMsg.Text = "Problemas na Leitura do template. Chamado Salvo." + ex01.Message;
                    //LabelMsg.Visible = true;
                    return;
                }

                StringBuilder txt = new StringBuilder();

                string linha = null;
                while ((linha = objReader.ReadLine()) != null)
                {
                    linha = linha.Replace("#NOME", ct.Solicitante.Nome);
                    linha = linha.Replace("#SERVICO", ct.TipoChamado.Descricao);
                    linha = linha.Replace("#CODIGO", ct.Codigo);
                    linha = linha.Replace("#SERVICO", ct.TipoChamado.Descricao);
                    linha = linha.Replace("#PATRIMONIO", ct.Patrimonio);
                    linha = linha.Replace("#DEPTO", ct.Secao.Sigla);
                    linha = linha.Replace("#LOCAL", ct.Local);
                    linha = linha.Replace("#ATENDENTE", ct.Atendente.ToString());
                    linha = linha.Replace("#ASSUNTO", ct.Assunto);
                    linha = linha.Replace("#CHAMADO", ct.Detalhes[0].interacao);
                    linha = linha.Replace("#E-MAIL", ct.Solicitante.Email);

                    txt.AppendLine(linha);
                }

                objReader.DiscardBufferedData();
                objReader.Dispose();
                objReader.Close();

                string emailSolic = ct.Solicitante.Email + ",fernando.lq@usp.br ";
                if (!string.IsNullOrEmpty(DropDownListAtendente.SelectedValue))
                {
                    emailSolic += "," + ct.Atendente.Email;
                }
                //envio para o solicitante
                try
                {
                    string assunto = "Abertura de Chamado Técnico de Informática - Sistema SiCEI - SCINFOR - PUSP-LQ";

                    EmailEnvioBO.Enviar(assunto, txt.ToString(), emailSolic, "sti.lq@usp.br");
                }
                catch (Exception ex01)
                {
                    //LabelMsg.Text = "Problemas no envio. Chamado Salvo." + ex01.Message;
                    //LabelMsg.Visible = true;
                    return;
                }

                #endregion 
            }
        }

        protected void DropDownListAtendente_PreRender(object sender, EventArgs e)
        {
            DropDownListAtendente.Items.FindByValue(PessoaBO.getUIDFromUserName(User.Identity.Name)).Selected = true;
        }

    }
}
