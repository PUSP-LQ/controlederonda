﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CamadaSQLServer.Orbita;
using DevExpress.Web.ASPxNavBar;
using System.IO;
using System.Text;
using System.Threading;

namespace SistemasOnline.SiCEI
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        public string userhost;
        public string username;

        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {
            Login login = sender as Login;

            e.Authenticated = UsuarioBO.Identifica(login.UserName, login.Password);

            userhost = Request.UserHostAddress;
            username = login.UserName;

            try
            {
                if (e.Authenticated)
                {
                    new Thread(delegate()
                    {
                        new UsuarioDAO().registerLog("login_projetos", username, userhost, string.Empty);
                    }
                        ).Start();
                }
            }
            catch (Exception)
            {

                //throw;
            }
        }
    }
}
