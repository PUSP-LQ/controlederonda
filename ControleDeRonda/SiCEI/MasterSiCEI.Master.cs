﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxNavBar;
using CamadaSQLServer.Orbita;

namespace SistemasOnline.SiCEI
{
    public partial class MasterSiCEI : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Page.Request.Url.Query.Contains("aval"))
            {
                ASPxMenu1.Enabled = false;
            } else
            if (!this.Page.User.Identity.IsAuthenticated )
            {
                Server.Transfer("default.aspx");                
            }
        }
    }

}
