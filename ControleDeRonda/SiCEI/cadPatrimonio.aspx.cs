﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CamadaSQLServer.Orbita;
using CamadaSQLServer.SiCEI;

namespace SistemasOnline.SiCEI
{
    public partial class cadPatrimonio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.Name != "7023777")
            {
                Response.End();
            }
        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (RadioButtonListMaterial.SelectedValue)
            {
                case "Monitor":
                    MultiView1.ActiveViewIndex = 0;
                    break;
                case "Microcomputador":
                    MultiView1.ActiveViewIndex = 1;
                    break;
                case "Impressora":
                    MultiView1.ActiveViewIndex = 2;
                    break;
                case "Nobreak":
                    MultiView1.ActiveViewIndex = 3;
                    break;
                default:
                    MultiView1.ActiveViewIndex = 3;
                    break;
            }
        }

        protected void ButtonBuscarPatrimonio_Click(object sender, EventArgs e)
        {
            
            PatrimonioTO patrimonioatual = new PatrimonioDAO().getAll(TextBoxPatrimonio.Text.Trim()).Last<PatrimonioTO>();

            if (!string.IsNullOrEmpty(patrimonioatual.Codigo))
            {
                RadioButtonListMaterial.Items.FindByValue(patrimonioatual.BemMaterial).Selected = true;
                RadioButtonListMaterial.Enabled = false;
                MultiView1.Visible = false;

                LabelEncontrado.Text = "Encontrado: Patrimônio: <br> <font color=\"black\">" + patrimonioatual.Codigo ;
                LabelEncontrado.Text += "<br> Atualmente com: " + PessoaBO.getPessoaFromUID(patrimonioatual.UIDPessoa).Nome;
                LabelEncontrado.Text += "<br> Seção: " + new DVCampusDAO().getSecao(patrimonioatual.CodSecao).Descricao +  " Local: " +  patrimonioatual.Local;
                LabelEncontrado.Text += "</font><br>";
            }

        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridView grd = sender as GridView;
            LabelId.Text = grd.SelectedRow.Cells[1].Text;

            DropDownListUnidade.DataBind();
        }

        protected void DropDownListUnidade_DataBound(object sender, EventArgs e)
        {

            PessoaTO usuario;

            if (string.IsNullOrEmpty(LabelId.Text.Trim()))
            {
                usuario = PessoaBO.getPessoaFromUserName(this.Page.User.Identity.Name);
            }
            else
            {
                usuario = PessoaBO.getPessoaFromID(Convert.ToInt32(LabelId.Text));
            }

            string unidade = new DVCampusDAO().getUnidade(usuario.Secao);

            if (!string.IsNullOrEmpty(unidade))
            {
                DropDownListUnidade.Items.FindByValue(unidade + "%").Selected = true;
            }
        }

        protected void DropDownListSecao_DataBound(object sender, EventArgs e)
        {

            PessoaTO usuario;

            if (string.IsNullOrEmpty(LabelId.Text.Trim()))
            {
                usuario = PessoaBO.getPessoaFromUserName(this.Page.User.Identity.Name);
            }
            else
            {
                usuario = PessoaBO.getPessoaFromID(Convert.ToInt32(LabelId.Text));
            }

            string secao = usuario.Secao;

            if (!string.IsNullOrEmpty(secao))
            {
                try
                {
                    DropDownListSecao.Items.FindByValue(secao).Selected = true;
                }
                catch (Exception z)
                {

                }

            }

        }

        protected void ButtonSalvar_Click(object sender, EventArgs e)
        {
            PatrimonioTO patrimonio = new PatrimonioTO();
            patrimonio.BemMaterial = RadioButtonListMaterial.SelectedValue;
            patrimonio.CodSecao = DropDownListSecao.SelectedValue;
            patrimonio.Codigo = TextBoxPatrimonio.Text;
            patrimonio.Local = TextBoxLocal.Text;
            patrimonio.UIDPessoa = PessoaBO.getPessoaFromID(Convert.ToInt32(LabelId.Text)).UID;
            patrimonio.Observacao = TextBoxObservacao.Text;

            List<PatrimonioDetalheTO> lista = new List<PatrimonioDetalheTO>();

            switch (RadioButtonListMaterial.SelectedValue)
            {
                case "Monitor":
                    lista.Add(new PatrimonioDetalheTO() { CodigoPatrimonio = patrimonio.Codigo, Caracteristica = "Marca", Valor = TextBoxMonitorMarca.Text });
                    lista.Add(new PatrimonioDetalheTO() { CodigoPatrimonio = patrimonio.Codigo, Caracteristica = "Polegada", Valor = TextBoxMonitorPolegada.Text });
                    lista.Add(new PatrimonioDetalheTO() { CodigoPatrimonio = patrimonio.Codigo, Caracteristica = "Wide", Valor = CheckBoxMonitoWide.Checked.ToString() });
                    break;
                case "Microcomputador":
                    lista.Add(new PatrimonioDetalheTO() { CodigoPatrimonio = patrimonio.Codigo, Caracteristica = "Marca", Valor = TextBoxMicroComputadorMarca.Text });
                    lista.Add(new PatrimonioDetalheTO() { CodigoPatrimonio = patrimonio.Codigo, Caracteristica = "SistemaOperacional", Valor = RadioButtonListMicroComputadorSistemaOperacional.SelectedValue });
                    lista.Add(new PatrimonioDetalheTO() { CodigoPatrimonio = patrimonio.Codigo, Caracteristica = "Office", Valor = RadioButtonListMicroComputadorOffice.SelectedValue });
                    lista.Add(new PatrimonioDetalheTO() { CodigoPatrimonio = patrimonio.Codigo, Caracteristica = "MemoriaRam", Valor = TextBoxMicroComputadorMemRam.Text });
                    lista.Add(new PatrimonioDetalheTO() { CodigoPatrimonio = patrimonio.Codigo, Caracteristica = "HardDisk", Valor = TextBoxMicroComputadorHD.Text });
                    lista.Add(new PatrimonioDetalheTO() { CodigoPatrimonio = patrimonio.Codigo, Caracteristica = "RedeSemFio", Valor = CheckBoxMicroComputadorRedeSemFio.Checked.ToString() });
                    lista.Add(new PatrimonioDetalheTO() { CodigoPatrimonio = patrimonio.Codigo, Caracteristica = "IP", Valor = TextBoxMicroComputadorIP.Text });
                    lista.Add(new PatrimonioDetalheTO() { CodigoPatrimonio = patrimonio.Codigo, Caracteristica = "Backup", Valor = CheckBoxMicroComputadorBackup.Checked.ToString() });
                    lista.Add(new PatrimonioDetalheTO() { CodigoPatrimonio = patrimonio.Codigo, Caracteristica = "Processador", Valor = TextBoxMicroComputadorProcessador.Text });
                    lista.Add(new PatrimonioDetalheTO() { CodigoPatrimonio = patrimonio.Codigo, Caracteristica = "Modelo", Valor = TextBoxMicroComputadorModelo.Text });                    
                    
                    break;
                case "Impressora":
                    foreach (ListItem item in CheckBoxListImpressora.Items)
                    {
                        if (item.Selected)
                        {
                            lista.Add(new PatrimonioDetalheTO() { CodigoPatrimonio = patrimonio.Codigo, Caracteristica = item.Text.ToUpper(), Valor = item.Value });        
                        }
                    }                    
                    break;
                case "Nobreak":
                    lista.Add(new PatrimonioDetalheTO() { CodigoPatrimonio = patrimonio.Codigo, Caracteristica = "", Valor = "" });
                    break;
                default:
                    break;
            }

            patrimonio.Caracteristicas = lista;
            patrimonio.UserCreate = this.User.Identity.Name;
            patrimonio.xHost = Request.UserHostAddress;

            new PatrimonioDAO().Salvar(patrimonio);

            LabelMsg.Text = "Salvo com sucesso.";

            //limpa controles
            RadioButtonListMaterial.ClearSelection();
            TextBoxPatrimonio.Text = "";
            TextBoxLocal.Text = "";
            TextBoxMonitorMarca.Text = "";
            TextBoxMonitorPolegada.Text = "";
            TextBoxMicroComputadorMarca.Text = "";
            RadioButtonListMicroComputadorSistemaOperacional.ClearSelection();
            TextBoxMicroComputadorMemRam.Text = "";
            TextBoxMicroComputadorHD.Text = "";
            CheckBoxMicroComputadorRedeSemFio.Text = "";
            TextBoxMicroComputadorIP.Text = "";
            CheckBoxMicroComputadorBackup.Checked = false;


        }

        protected void RadioButtonListAcao_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (RadioButtonListAcao.SelectedValue)
            {
                case "registro":
                    MultiView2.ActiveViewIndex = 0;
                    break;
                case "baixa":
                    MultiView2.ActiveViewIndex = 0;
                    break;
                default:
                    MultiView2.ActiveViewIndex = 1;
                    break;
            }
        }

    }
}
