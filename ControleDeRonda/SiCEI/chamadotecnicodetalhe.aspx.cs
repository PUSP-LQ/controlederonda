﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CamadaSQLServer.SiCEI;
using CamadaSQLServer.Orbita;
using System.IO;
using System.Text;

namespace SistemasOnline.SiCEI
{
    public partial class chamadotecnicodetalhe : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PanelInteracao.Visible = this.Page.User.Identity.IsAuthenticated;

            if (string.IsNullOrEmpty(Request["code"]))
            {
                Response.End();
            }

            string uidchamado = Request["code"].ToString();
            ChamadoTecnicoDAO DAO = new ChamadoTecnicoDAO();
            ChamadoTecnicoTO chamado = new ChamadoTecnicoTO(); //DAO.getFromCode(uidchamado);
           
            LabelAssunto.Text = chamado.Assunto;
            LabelAtendente.Text = chamado.Atendente.ToString();
            LabelCodigo.Text = chamado.Codigo;
            LabelPatrimonio.Text =chamado.Patrimonio;
            LabelSecao.Text = chamado.Secao.ToString();
            LabelServico.Text = chamado.TipoChamado.ToString();
            LabelSolicitante.Text = chamado.NomeSolicitante; 
            LabelUIDChamado.Text = chamado.UID;


            if (chamado.MomFechamento.CompareTo(new DateTime()) != 0)
            {
                MultiView1.ActiveViewIndex = 2;
            }

            if (this.Page.Request.Url.Query.Contains("aval"))
            {
                string aval = Request["aval"].ToString();

                string MD5codigochamado = UtilBO.GetMD5Hash(chamado.Codigo);

                if (MD5codigochamado == aval)
                {
                    if (!DAO.jaFoiAvaliadoQualidade(chamado.Codigo))
                    {

                        MultiView1.ActiveViewIndex = 3;
                    }
                    else
                    {
                        MultiView1.ActiveViewIndex = 5;
                    }

                }
               
            }
        }

        protected void ButtonSalvar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request["code"]))
            {
                string codigochamado = Request["code"].ToString();

                string interacao = string.Empty;

                if (!string.IsNullOrEmpty(DropDownListAtendente.SelectedValue))
                {
                    interacao = ". Encaminhado para " + DropDownListAtendente.SelectedItem;                    
                };

                ChamadoTecnicoTO chamado = new ChamadoTecnicoDAO().getFromCode(codigochamado);

                ChamadoTecnicoDetalheTO detalhe = new ChamadoTecnicoDetalheTO();
                ChamadoTecnicoDAO chamadoDAO = new ChamadoTecnicoDAO();
                detalhe.autor = PessoaBO.getPessoaFromUserName(this.User.Identity.Name);
                detalhe.chamado = chamado.UID;
                detalhe.contador = chamadoDAO.getContador(chamado.UID) + 1;
                detalhe.etapa = 1;
                detalhe.interacao = TextBoxInteracao.Text + interacao;
                detalhe.momento = DateTime.Now;
                detalhe.uid = Guid.NewGuid().ToString();
                detalhe.xHOST_SOLIC = this.Page.Request.UserHostName;
                detalhe.xIP_SOLIC = this.Page.Request.UserHostAddress;
                
                chamadoDAO.Salvar(detalhe);

                if (!string.IsNullOrEmpty(DropDownListAtendente.SelectedValue))
                {
                    chamadoDAO.updateAtendente(chamado.UID, DropDownListAtendente.SelectedValue);
                    
                    StreamReader objReader;

                    //envio para o solicitante
                    try
                    {
                        objReader = new StreamReader(Request.PhysicalApplicationPath + @"\Template\Email\AvisoEncaminhamentoChamadoTecnico.html");
                    }
                    catch (Exception ex01)
                    {
                        //LabelMsg.Text = "Problemas na Leitura do template. Chamado Salvo." + ex01.Message;
                        //LabelMsg.Visible = true;
                        return;
                    }

                    StringBuilder txt = new StringBuilder();

                    string linha = null;
                    while ((linha = objReader.ReadLine()) != null)
                    {
                        linha = linha.Replace("#NOME", chamado.Atendente.ToString());
                        linha = linha.Replace("#CODIGO", chamado.Codigo);
                        linha = linha.Replace("#SERVICO", chamado.TipoChamado.Descricao);
                        linha = linha.Replace("#PATRIMONIO", chamado.Patrimonio);
                        linha = linha.Replace("#DEPTO", chamado.Secao.Sigla);
                        linha = linha.Replace("#LOCAL", chamado.Local);
                        linha = linha.Replace("#ATENDENTE", chamado.Atendente.ToString());
                        linha = linha.Replace("#ASSUNTO", chamado.Assunto);
                        linha = linha.Replace("#CHAMADO", chamado.Detalhes[0].interacao);
                        linha = linha.Replace("#E-MAIL", chamado.Solicitante.Email);

                        txt.AppendLine(linha);
                    }

                    objReader.DiscardBufferedData();
                    objReader.Dispose();
                    objReader.Close();

                    string emailSolic = chamado.Atendente.Email + ", fernando.lq@usp.br ";
                    //envio para o solicitante
                    try
                    {
                        string assunto = "Encaminhamento de Chamado Técnico de Informática para Atendimento - SiCEI - SCINFOR - PUSP-LQ";

                        EmailEnvioBO.Enviar(assunto, txt.ToString(), emailSolic, "sti.lq@usp.br");
                    }
                    catch (Exception ex01)
                    {
                        //LabelMsg.Text = "Problemas no envio. Chamado Salvo." + ex01.Message;
                        //LabelMsg.Visible = true;
                        return;
                    }

                }

                TextBoxInteracao.Text = string.Empty;
            }
        }

        protected void ButtonSalvarFechar_Click(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(Request["code"]))
            {
                string codigochamado = Request["code"].ToString();

                if (!string.IsNullOrEmpty(DropDownListAtendente.SelectedValue))
                {
                    LabelMsg.Text = "Não é possível encaminhar e fechar.";
                    return;
                }

                if (CheckBoxRisca.Checked)
                {
                    LabelMsg.Text = "Não é possível riscar e fechar.";
                    return;
                }                                   

                ChamadoTecnicoTO chamado = new ChamadoTecnicoDAO().getFromCode(codigochamado);

                ChamadoTecnicoDetalheTO detalhe = new ChamadoTecnicoDetalheTO();
                ChamadoTecnicoDAO chamadoDAO = new ChamadoTecnicoDAO();
                detalhe.autor = PessoaBO.getPessoaFromUserName(this.User.Identity.Name);
                detalhe.chamado = chamado.UID;
                detalhe.contador = chamadoDAO.getContador(chamado.UID) + 1;
                detalhe.etapa = 1;
                detalhe.interacao = TextBoxInteracao.Text;
                detalhe.momento = DateTime.Now;
                detalhe.uid = Guid.NewGuid().ToString();
                detalhe.xHOST_SOLIC = this.Page.Request.UserHostName;
                detalhe.xIP_SOLIC = this.Page.Request.UserHostAddress;

                chamadoDAO.SalvareFechar(detalhe);

                TextBoxInteracao.Text = string.Empty;

                StreamReader objReader;

                //envio para o solicitante
                try
                {
                    objReader = new StreamReader(Request.PhysicalApplicationPath + @"\Template\Email\AvisoChamadoTecnicoFinalizado.html");
                }
                catch (Exception ex01)
                {
                    //LabelMsg.Text = "Problemas na Leitura do template. Chamado Salvo." + ex01.Message;
                    //LabelMsg.Visible = true;
                    return;
                }

                string url = Request.Url.AbsoluteUri;
                url = url.Substring(0, url.LastIndexOf("/"));
                url = url.Substring(0, url.LastIndexOf("/")) + "/sicei/chamadotecnicodetalhe.aspx?code=" + chamado.Codigo + "&aval=" + UtilBO.GetMD5Hash(chamado.Codigo);

                StringBuilder txt = new StringBuilder();

                string linha = null;
                while ((linha = objReader.ReadLine()) != null)
                {
                    linha = linha.Replace("#NOME", chamado.Atendente.ToString());
                    linha = linha.Replace("#CODIGO", chamado.Codigo);
                    linha = linha.Replace("#SERVICO", chamado.TipoChamado.Descricao);
                    linha = linha.Replace("#PATRIMONIO", chamado.Patrimonio);
                    linha = linha.Replace("#DEPTO", chamado.Secao.Sigla);
                    linha = linha.Replace("#LOCAL", chamado.Local);
                    linha = linha.Replace("#ATENDENTE", chamado.Atendente.ToString());
                    linha = linha.Replace("#ASSUNTO", chamado.Assunto);
                    linha = linha.Replace("#CHAMADO", chamado.Detalhes[0].interacao);
                    linha = linha.Replace("#E-MAIL", chamado.Solicitante.Email);
                    linha = linha.Replace("#ULTIMOPARECER", chamado.Detalhes.Last().interacao);
                    linha = linha.Replace("#LINKAVALIAR", url);

                    txt.AppendLine(linha);
                }

                objReader.DiscardBufferedData();
                objReader.Dispose();
                objReader.Close();

                string emailSolic = chamado.Atendente.Email + ", fernando.lq@usp.br ";
                //envio para o solicitante
                try
                {
                    string assunto = "Encerramento de Chamado Técnico de Informática para Atendimento - SiCEI - SCINFOR - PUSP-LQ";

                    EmailEnvioBO.Enviar(assunto, txt.ToString(), emailSolic, "sti.lq@usp.br");
                }
                catch (Exception ex01)
                {
                    //LabelMsg.Text = "Problemas no envio. Chamado Salvo." + ex01.Message;
                    //LabelMsg.Visible = true;
                    return;
                }

                MultiView1.ActiveViewIndex = 2;

            }
        }

        protected void ButtonSalvarQualidade_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Request["code"]) && this.Page.Request.Url.Query.Contains("aval"))
            {
                string codigochamado = Request["code"].ToString();
                string aval = Request["aval"].ToString();

                string MD5codigochamado = UtilBO.GetMD5Hash(codigochamado);

                if (MD5codigochamado == aval)
                {
                    ChamadoTecnicoDAO DAO = new ChamadoTecnicoDAO();
                    if (!DAO.jaFoiAvaliadoQualidade(codigochamado))
                    {
                        ChamadoTecnicoTO chamado = new ChamadoTecnicoDAO().getFromCode(codigochamado);
                        new ChamadoTecnicoDAO().updateQualidade(codigochamado, RadioButtonListTempo.SelectedValue, RadioButtonListQualidade.SelectedValue, TextBoxComentario.Text);

                        MultiView1.ActiveViewIndex = 5;

                        StreamReader objReader;

                        //envio para o atendente
                        try
                        {
                            objReader = new StreamReader(Request.PhysicalApplicationPath + @"\Template\Email\AvisoResultadoAvaliacaoQualidade.txt");
                        }
                        catch (Exception ex01)
                        {
                            //LabelMsg.Text = "Problemas na Leitura do template. Chamado Salvo." + ex01.Message;
                            //LabelMsg.Visible = true;
                            return;
                        }

                        StringBuilder txt = new StringBuilder();

                        string linha = null;
                        while ((linha = objReader.ReadLine()) != null)
                        {
                            linha = linha.Replace("#NOME", chamado.Atendente.ToString());
                            linha = linha.Replace("#QUALIDADETEMPO", RadioButtonListTempo.SelectedValue);
                            linha = linha.Replace("#QUALIDADESOLUCAO", RadioButtonListQualidade.SelectedValue);
                            linha = linha.Replace("#QUALIDADECOMENTARIO", TextBoxComentario.Text);

                            txt.AppendLine(linha);
                        }

                        objReader.DiscardBufferedData();
                        objReader.Dispose();
                        objReader.Close();

                        string emailSolic = chamado.Atendente.Email + ", fernando.lq@usp.br ";
                        //envio para o solicitante
                        try
                        {
                            string assunto = "Encerramento de Chamado Técnico de Informática para Atendimento - SiCEI - SCINFOR - PUSP-LQ";

                            EmailEnvioBO.Enviar(assunto, txt.ToString(), emailSolic, "sti.lq@usp.br");
                        }
                        catch (Exception ex01)
                        {
                            //LabelMsg.Text = "Problemas no envio. Chamado Salvo." + ex01.Message;
                            //LabelMsg.Visible = true;
                            return;
                        }


                    }
                    else
                    {
                        MultiView1.ActiveViewIndex = 6;
                    }
                }
                else
                {
                    Label1.Text = "Código Inválido.";
                }
            }
        }
    }
}
