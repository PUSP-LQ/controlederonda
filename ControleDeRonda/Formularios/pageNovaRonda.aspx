<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterOrbita.Master" AutoEventWireup="true" CodeBehind="pageNovaRonda.aspx.cs" Inherits="ControleDeRonda.Formularios.pageNovaRonda" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="LabelMsgTop" runat="server" Text="Salvado com sucesso" Visible="false"></asp:Label>
    <br /><br />
    <table style="width:90%;">
        <tr>
            <td>
                <asp:Label ID="LabelTurno" runat="server" Text="Turno in�ciado em:" Visible="false"></asp:Label></td>
            <td>
                <asp:Label ID="LabelData" runat="server" Visible="false"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Nome do Contr. R�dio:</td>
            <td>
                <asp:TextBox ID="TextBoxNomeControleRadio" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;&nbsp; &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="ButtonTurno" runat="server" Text="Iniciar turno" 
                    onclick="ButtonTurno_Click" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td align="center">
                &nbsp;</td>
        </tr>
        </table>
        <br />
    <asp:Panel ID="PanelRonda" runat="server" Visible="false">
   
    <br />
        <table style="width:90%;">
        <tr>
            <td>
                Equipe:</td>
            <td>
                <asp:TextBox ID="TextBoxEquipe" runat="server"></asp:TextBox>
            </td>
            <td>
                Ronda de</td>
            <td>
                <asp:DropDownList ID="DropDownListRonda" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DropDownListRonda_SelectedIndexChanged">
                    <asp:ListItem>VTR</asp:ListItem>
                    <asp:ListItem>Mike</asp:ListItem>
                    <asp:ListItem>Bike</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <!-- tr>
            <td>
                Hor�rio:</td>
            <td>
                <asp:TextBox ID="TextBoxHorario" runat="server" visible="false"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr -->
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        </table>
    <br /><br />
    
    <table style="width: 90%;">
        <tr>
            <td>
                <asp:Label ID="LabelRonda01" runat="server" Text="VTR 01"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxLocal01" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelRonda02" runat="server" Text="VTR 02"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxLocal02" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelRonda03" runat="server" Text="VTR 03"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxLocal03" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelRonda04" runat="server" Text="VTR 04"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxLocal04" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;&nbsp; &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                    <asp:Button ID="ButtonSalvar" runat="server" Text="Salvar" 
                    onclick="ButtonSalvar_Click" />
            </td>
        </tr>
    </table> 
</asp:Panel>    
    <br />
    <asp:Label ID="LabelMsgDown" runat="server" Text="Salvado com sucesso" Visible="false"></asp:Label>
    <br />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        DataSourceID="ObjectDataSourceRegistros" EnableViewState="false">
        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" />
            <asp:BoundField DataField="MOMCAD" HeaderText="Hor�rio" 
                SortExpression="MOMCAD" />
            <asp:BoundField DataField="RondaVeiculo" HeaderText="Ve�culo" 
                SortExpression="RondaVeiculo" />
            <asp:BoundField DataField="Local01" HeaderText="Local01" 
                SortExpression="Local01" />
            <asp:BoundField DataField="Local02" HeaderText="Local02" 
                SortExpression="Local02" />
            <asp:BoundField DataField="Local03" HeaderText="Local03" 
                SortExpression="Local03" />
            <asp:BoundField DataField="Local04" HeaderText="Local04" 
                SortExpression="Local04" />
        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource ID="ObjectDataSourceRegistros" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="getRegistrosTurno" 
        TypeName="CamadaSQLServer.Guarda.RondaRegistrosBO">
        <SelectParameters>
            <asp:SessionParameter Name="uidTurno" SessionField="turnoUID" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    </asp:Content>
