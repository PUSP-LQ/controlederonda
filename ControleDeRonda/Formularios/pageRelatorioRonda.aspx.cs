﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ControleDeRonda.Formularios
{
    public partial class pageRelatorioRonda : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sql = "SELECT * FROM [gu_RondaTurnoTO] WHERE 1 = 1";

            SqlDataSourceTurnos.SelectParameters.Clear();

            if (!string.IsNullOrEmpty(TextBoxDtInicio.Text))
            {
                sql += " and TurnoInicio >= @turnoinicio";
                SqlDataSourceTurnos.SelectParameters.Add(new ControlParameter("turnoinicio", System.Data.DbType.DateTime, "TextBoxDtInicio", "Text"));
            }
            if (!string.IsNullOrEmpty(TextBoxDtFim.Text))
            {
                sql += " and TurnoFim < @turnofim";
                SqlDataSourceTurnos.SelectParameters.Add(new ControlParameter("turnofim", System.Data.DbType.DateTime, "TextBoxDtFim", "Text"));
            }
            if (!string.IsNullOrEmpty(TextBoxNomeContrRadio.Text))
            {
                TextBoxNomeContrRadio.Text = "%" + TextBoxNomeContrRadio.Text + "%";
                sql += " and NomeContrRadio like @NomeContrRadio";
                ControlParameter cp = new ControlParameter("NomeContrRadio", System.Data.DbType.String, "TextBoxNomeContrRadio", "Text");
                SqlDataSourceTurnos.SelectParameters.Add(cp);
            }
            //if (!string.IsNullOrEmpty(TextBoxLocal.Text))
            //{
            //    TextBoxLocal.Text = "%" + TextBoxLocal.Text + "%";
            //    sql += " and os.assunto like @assunto";
            //    ControlParameter cp = new ControlParameter("assunto", System.Data.DbType.String, "TextBoxAssunto", "Text");
            //    SqlDataSource1.SelectParameters.Add(cp);
            //}
            //if (!string.IsNullOrEmpty(DropDownListVeiculo.SelectedIndex > 0))
            //{
            //    TextBoxAssunto.Text = "%" + TextBoxAssunto.Text + "%";
            //    sql += " and os.assunto like @assunto";
            //    ControlParameter cp = new ControlParameter("assunto", System.Data.DbType.String, "TextBoxAssunto", "Text");
            //    SqlDataSource1.SelectParameters.Add(cp);
            //}
                
                
            if (IsPostBack)
            {
                SqlDataSourceTurnos.SelectCommand = sql + " ORDER BY ID DESC";    
            }

        }
    }
}
