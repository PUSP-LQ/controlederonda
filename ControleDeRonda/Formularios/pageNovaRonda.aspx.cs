﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CamadaSQLServer.Orbita;
using ControleDeRonda.Camadas;

namespace ControleDeRonda.Formularios
{
    public partial class pageNovaRonda : System.Web.UI.Page
    {
        RondaTurnoTO turno;
        
        protected void Page_Load(object sender, EventArgs e)
        {
             // tem de verificar se o turno está aberto.

            LabelData.Text = DateTime.Now.ToShortDateString();
            TextBoxHorario.Text = DateTime.Now.ToShortTimeString();
            LabelMsgTop.Visible = false;

            if (RondaTurnoBO.isOpen())
            {
                turno = RondaTurnoDAO.getLastTurno();
                Session["rondaAberta"] = turno;
                Session["turnoUID"] = turno.UID;
                PanelRonda.Visible = true;

                LabelData.Text = turno.TurnoInicio.ToLongDateString() + " " + turno.TurnoInicio.ToLongTimeString();
                TextBoxNomeControleRadio.Text = turno.NomeContrRadio;
                TextBoxNomeControleRadio.Enabled = false;
                ButtonTurno.Text = "Parar turno";
                LabelTurno.Visible = true;
                LabelData.Visible = true;
            } 
                else 
            {
                PanelRonda.Visible = false;
                LabelTurno.Visible = false;
                LabelData.Visible = false;
            }            
            

        }

        protected void ButtonTurno_Click(object sender, EventArgs e)
        {
            //grava dados para iniciar turno

            if (turno != null && turno.ID > 0)
            {
                //stop de turn
                ButtonTurno.Text = "Iniciar turno";
                PanelRonda.Visible = false;
                RondaTurnoTO turnoAberto = Session["rondaAberta"] as RondaTurnoTO;
                turnoAberto.TurnoFim = DateTime.Now;
                turnoAberto.MOMALT = DateTime.Now;
                RondaTurnoDAO.Update(turnoAberto);

                //apresentar grid dos turnos

                LabelTurno.Visible = false;
                LabelData.Visible = false;
                TextBoxNomeControleRadio.Text = string.Empty;
                TextBoxNomeControleRadio.Enabled = true;

            }
            else
            {

                if (string.IsNullOrEmpty(TextBoxNomeControleRadio.Text))
                {
                    LabelMsgTop.Text = "Nome do controlado está vazio.";
                    LabelMsgTop.Visible = true;
                        return;
                }
                turno = new RondaTurnoTO();
                turno.MOMALT = DateTime.Now;
                turno.MOMCAD = DateTime.Now;
                turno.NomeContrRadio = TextBoxNomeControleRadio.Text;
                turno.TurnoInicio = DateTime.Now;
                turno.UID = Guid.NewGuid().ToString();
                turno.UsuarioLogadoNome = TextBoxNomeControleRadio.Text;
                turno.UsuarioLogadoUID = string.Empty;

                RondaTurnoDAO.Insert(turno);

                Session["rondaAberta"] = turno;
                Session["turnoUID"] = turno.UID;

                PanelRonda.Visible = true;
                ButtonTurno.Text = "Parar turno";

                PanelRonda.Visible = true;

                LabelData.Text = turno.TurnoInicio.ToLongDateString() + " " + turno.TurnoInicio.ToLongTimeString();
                TextBoxNomeControleRadio.Text = turno.NomeContrRadio;
                TextBoxNomeControleRadio.Enabled = false;
                ButtonTurno.Text = "Parar turno";
                LabelTurno.Visible = true;
                LabelData.Visible = true;

            }
        }

        protected void DropDownListRonda_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (DropDownListRonda.SelectedValue)
            {
                case "VTR":
                    LabelRonda01.Text = "VTR 01";
                    LabelRonda02.Text = "VTR 02";
                    LabelRonda03.Text = "VTR 03";
                    LabelRonda04.Text = "VTR 04";
                    break;
                case "Mike":
                    LabelRonda01.Text = "Mike 41";
                    LabelRonda02.Text = "Mike 42";
                    LabelRonda03.Text = "Mike 43";
                    LabelRonda04.Text = "Mike 44";
                    break;
                case "Bike":
                    LabelRonda01.Text = "Bike 01";
                    LabelRonda02.Text = "Bike 02";
                    LabelRonda03.Text = "Bike 03";
                    LabelRonda04.Text = "Bike 04";
                    break;
                default:
                    break;
            }
        }

        protected void ButtonSalvar_Click(object sender, EventArgs e)
        {
            RondaRegistrosTO registro = new RondaRegistrosTO();
            
            RondaTurnoTO turnoAberto = Session["rondaAberta"] as RondaTurnoTO;
            registro.RondaTurnoUID = turnoAberto.UID;
            registro.MOMALT = DateTime.Now;
            
            registro.RondaEquipe = TextBoxEquipe.Text;
            registro.RondaVeiculo = DropDownListRonda.SelectedValue;

            registro.UsuarioLogadoNome = TextBoxNomeControleRadio.Text;
            registro.UsuarioLogadoUID = string.Empty;

            registro.Local01 = TextBoxLocal01.Text;
            registro.Local02 = TextBoxLocal02.Text;
            registro.Local03 = TextBoxLocal03.Text;
            registro.Local04 = TextBoxLocal04.Text;

            RondaRegistrosDAO.Insert(registro);

            //limpa os controles e lanca o proximo horario

             TextBoxLocal01.Text = string.Empty;
             TextBoxLocal02.Text = string.Empty;
             TextBoxLocal03.Text = string.Empty;
             TextBoxLocal04.Text = string.Empty;

             LabelMsgTop.Visible = true;
             LabelMsgTop.Text = "Gravado com sucesso.";
        }
    }
}
