<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterOrbita.Master" AutoEventWireup="true" CodeBehind="pageRelatorioRonda.aspx.cs" Inherits="ControleDeRonda.Formularios.pageRelatorioRonda" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <table style="width: 90%;">
        <tr>
            <td>
                &nbsp;<asp:Label ID="LabelDtInicio" runat="server" Text="Data in�cio"></asp:Label>
                :</td>
            <td>
                <asp:TextBox ID="TextBoxDtInicio" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;<asp:Label ID="LabelDtFim" runat="server" Text="Data Fim"></asp:Label>
                :</td>
            <td>
                <asp:TextBox ID="TextBoxDtFim" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;<asp:Label ID="LabelNomeContrRadio" runat="server" Text="Controle de Radio"></asp:Label>
                :</td>
            <td>
                <asp:TextBox ID="TextBoxNomeContrRadio" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Local:</td>
            <td>
                <asp:TextBox ID="TextBoxLocal" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Ve�culo:</td>
            <td>
                <asp:DropDownList ID="DropDownListVeiculo" runat="server">
                    <asp:ListItem></asp:ListItem>
                    <asp:ListItem>VTR</asp:ListItem>
                    <asp:ListItem>Mike</asp:ListItem>
                    <asp:ListItem>Bike</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="ButtonBuscar" runat="server" Text="Buscar" />
            </td>
        </tr>
    </table>

<br />
<br />

    <asp:GridView ID="GridViewTurnos" runat="server" Width="95%" 
        AutoGenerateColumns="False" DataSourceID="SqlDataSourceTurnos" DataKeyNames="UID">
        <Columns>
            <asp:CommandField ShowSelectButton="True" />
            <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" />
            <asp:BoundField DataField="UID" HeaderText="UID" SortExpression="UID" Visible="false" />
            <asp:BoundField DataField="TurnoInicio" HeaderText="Turno In�cio" 
                SortExpression="TurnoInicio" />
            <asp:BoundField DataField="TurnoFim" HeaderText="Turno Fim" 
                SortExpression="TurnoFim" />
            <asp:BoundField DataField="NomeContrRadio" HeaderText="Nome Controle R�dio" 
                SortExpression="NomeContrRadio" />
        </Columns>
    </asp:GridView>
    
    <asp:SqlDataSource ID="SqlDataSourceTurnos" runat="server" 
        ConnectionString="<%$ ConnectionStrings:oscclqConnectionString %>" 
        SelectCommand="SELECT * FROM [gu_RondaTurnoTO] WHERE 1=2 ">

    </asp:SqlDataSource>
  
    
    <br />
    <br />
    
    <asp:GridView ID="GridViewRegistros" runat="server" Width="95%" 
        AutoGenerateColumns="False" DataSourceID="ObjectDataSourceRegistros">
        <Columns>
            <asp:BoundField DataField="MOMCAD" HeaderText="Momento" SortExpression="MOMCAD" />                              
            <asp:BoundField DataField="RondaEquipe" HeaderText="Equipe" 
                SortExpression="RondaEquipe" />
            <asp:BoundField DataField="RondaVeiculo" HeaderText="Ve�culo" 
                SortExpression="RondaVeiculo" />
            <asp:BoundField DataField="Local01" HeaderText="Local 01" 
                SortExpression="Local01" />
            <asp:BoundField DataField="Local02" HeaderText="Local 02" 
                SortExpression="Local02" />
            <asp:BoundField DataField="Local03" HeaderText="Local 03" 
                SortExpression="Local03" />
            <asp:BoundField DataField="Local04" HeaderText="Local 04" 
                SortExpression="Local04" />
        </Columns>
    </asp:GridView>
    
    <asp:ObjectDataSource ID="ObjectDataSourceRegistros" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="getRegistrosTurno" 
        TypeName="CamadaSQLServer.Guarda.RondaRegistrosBO">
        <SelectParameters>
            <asp:ControlParameter ControlID="GridViewTurnos" Name="uidTurno" 
                PropertyName="SelectedValue" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    
</asp:Content>
